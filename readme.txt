Popis obsahu CD:
|- src..................................zdrojov� k�dy implementace
|    |- BP_Radek_Pus2019.tex............zdrojov� forma pr�ce ve form�tu LaTeX
|- publish..............................adres�� se spustitelnou formou implementace
|- text.................................text pr�ce
|    |- BP_Radek_Pus2019.pdf............text pr�ce ve form�tu PDF
|- P��lohy..............................p�ilo�en� soubory


Minim�ln� po�adavky pro spu�t�n� aplikace:
- Docker

Minim�ln� po�adavky na importn� soubor:
- form�t UTF-8 / Unicode
- odd�lova� ; nebo ,
- hlavi�ka souboru je z�rove� prn� ��dek
- z�znamy maj� vypln�no datum a ��stku


Spu�ten� aplikace:
1) otev��t libovoln� termin�l (cmd, powershell, bash,..)
2) p�ej�t do slo�ky "Publish"
3) zadat p��kaz:
	docker-compose up
4) v libovoln�m prohl�e�i p�ej�t na adresu:
	http://localhost/

Pozn.: p�i prvn�m spu�t�n� se pomoc� Dockeru stahuj� a nastavuj� pot�ebn� soubory. Toto spu�t�n� proto obvykle trv� 10-15 minut (v z�vislosti na rychlosti p�ipojen� a v�po�etn�ho v�konu po��ta�e). Dal�� spou�t�n� jsou v�ak t�m�� okam�it�.


V�choz� p��stupy do aplikace:
- v�echny ��ty maj� shodn� heslo "password" (bez uvozovek)
- je vytvo�eno celkem p�t ��t�:
	- csas		- v�pis z �esk� spo�itelny
	- csob		- v�pis z �SOB
	- airbank	- v�pis z Air Bank
	- trask		- data dodan� spole�nost� Trask Solutions
	- example	- p��klad ide�ln�ch dat

