﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransactionComparator.Interface
{
    public interface IElement
    {
        double Value { get; }
        bool Repetitive { set; }
    }
}
