﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransactionComparator.Interface
{
    interface ICompratorInput
    {
        List<IElement> First { get; }
        List<IElement> Second { get; }
    }
}
