﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransactionComparator.Logic
{
    class PairedTransactionContainer
    {
        public Vertex First;
        public Vertex Second;
        public readonly double Distance;

        private bool RepetivnessReseted = false;

        public PairedTransactionContainer(Vertex first, Vertex second)
        {
            first.Repetitive = second.Repetitive = true;

            First = first;
            Second = second;

            // make distance as ratio of two elements
            double sizeOfFirst = Math.Abs(First.Value);
            double sizeOfSecond = Math.Abs(Second.Value);

            Distance = sizeOfFirst > sizeOfSecond ? sizeOfFirst / sizeOfSecond : sizeOfSecond /sizeOfFirst;
        }

        public void ResetRepetitivness()
        {
            First.Repetitive = Second.Repetitive = false;
            RepetivnessReseted = true;
        }

        public override string ToString()
        {
            return $"{First.Value} -> {Second.Value} ({Distance}), noise: {RepetivnessReseted}";
        }
    }
}
