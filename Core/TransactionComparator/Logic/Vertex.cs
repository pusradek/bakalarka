﻿using System;
using System.Collections.Generic;
using System.Text;
using TransactionComparator.Interface;

namespace TransactionComparator.Logic
{
    class Vertex : IElement, IComparable
    {
        internal IElement Source { get; }
        public double Value => Source.Value;
        public bool Repetitive { set => Source.Repetitive = value; }

        public int? NeighbourIndex { get; private set; }
        private double NeighbourDistance;

        public void Clear()
        {
            NeighbourIndex = null;
            NeighbourDistance = 0;
        }

        public Vertex(IElement e)
        {
            Source = e;
        }

        public int CompareTo(object obj)
        {
            if (!(obj is IElement))
                throw new ArgumentException("Argument not allowed");

            IElement another = (IElement)obj;
            if (Value < another.Value)
                return -1;

            if (Value > another.Value)
                return 1;

            return 0;
        }

        public bool SetNeighbourIndex(int index, double value)
        {
            if (NeighbourIndex != null && NeighbourDistance <= value)
                return false;

            NeighbourIndex = index;
            NeighbourDistance = value;

            return true;
        }
    }
}
