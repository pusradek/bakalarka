﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransactionComparator.Interface;
using TransactionComparator.Logic;

namespace TransactionComparator
{
    public class Comparator
    {
        private List<Vertex> Minor;
        private List<Vertex> Major;
        private readonly bool Swapped;

        private List<PairedTransactionContainer> PairedTransactions;

        public (List<IElement>, List<IElement>) GetRepetitivness()
        {
            List<IElement> first = new List<IElement>(Minor.Count + PairedTransactions.Count);
            List<IElement> second = new List<IElement>(Major.Count + PairedTransactions.Count);

            foreach (Vertex e in Minor)
                first.Add(e.Source);
            foreach (Vertex e in Major)
                second.Add(e.Source);

            foreach (var pair in PairedTransactions)
            {
                first.Add(pair.First.Source);
                second.Add(pair.Second.Source);
            }

            return Swapped ? (second, first) : (first, second);
        }

        public Comparator(List<IElement> first, List<IElement> second)
        {
            if (first.Count < second.Count)
            {
                Swapped = false;
                Minor = GetVertexListFromIElements(first);
                Major = GetVertexListFromIElements(second);
            }
            else
            {
                Swapped = true;
                Minor = GetVertexListFromIElements(second);
                Major = GetVertexListFromIElements(first);
            }

            Minor.Sort();
            Major.Sort();

            PairedTransactions = new List<PairedTransactionContainer>(Minor.Count);
        }

        public void Compare()
        {
            while (Minor.Count > 0)
            {
                int hintPointer = 0;
                for (int i = 0; i < Minor.Count; i++)
                {
                    hintPointer = GetNeigbourPointer(i, hintPointer);
                    double distance = Math.Abs(Major[hintPointer].Value - Minor[i].Value);
                    Major[hintPointer].SetNeighbourIndex(i, distance);
                    Minor[i].SetNeighbourIndex(hintPointer, distance);
                }
                RemovePairs();
            }
        }

        internal void WritePairs()
        {
            foreach (var pair in PairedTransactions)
                Console.WriteLine(pair.ToString());
        }

        internal void WriteUndetermined()
        {
            Console.WriteLine("Minor:");
            foreach (IElement e in Minor)
                Console.WriteLine(e.Value);

            Console.WriteLine("Major:");
            foreach (IElement e in Major)
                Console.WriteLine(e.Value);
        }

        private void RemovePairs()
        {
            List<Vertex> uniqueMinor = new List<Vertex>(Minor.Count);

            for (int i = 0; i < Minor.Count; i++)
            {
                int majorIndex = (int) Minor[i].NeighbourIndex;
                if (Major[majorIndex] == null || Major[majorIndex].NeighbourIndex != i)
                {
                    Minor[i].Clear();
                    uniqueMinor.Add(Minor[i]);
                    continue;
                }

                PairedTransactions.Add(new PairedTransactionContainer(Minor[i], Major[majorIndex]));
                Major[majorIndex] = null;
            }

            List<Vertex> uniqueMajor = new List<Vertex>(Major.Count - (Minor.Count - uniqueMinor.Count) + 1);
            Minor = uniqueMinor;

            for (int i = 0; i < Major.Count; i++)
            {
                if (Major[i] == null)
                    continue;
                Major[i].Clear();
                uniqueMajor.Add(Major[i]);
            }

            Major = uniqueMajor;
        }

        private int GetNeigbourPointer(int minorPointer, int majorHintPointer)
        {
            if (Minor[minorPointer].Value == Major[majorHintPointer].Value)
                return majorHintPointer;

            double increaseLength;
            if (majorHintPointer + 1 < Major.Count)
                increaseLength = Math.Abs(Major[majorHintPointer].Value - Major[majorHintPointer + 1].Value);
            else
                return DecreaseLength(minorPointer, majorHintPointer);

            double decreaseLength;
            if (majorHintPointer > 0)
                decreaseLength = Math.Abs(Major[majorHintPointer].Value - Major[majorHintPointer - 1].Value);
            else
                return IncreaseLength(minorPointer, majorHintPointer);

            return increaseLength > decreaseLength ? DecreaseLength(minorPointer, majorHintPointer) : IncreaseLength(minorPointer, majorHintPointer);
        }

        #region helper methods
        private int IncreaseLength(int minorPointer, int majorHintPointer)
        {
            double minorValue = Minor[minorPointer].Value;
            double previousValue = Math.Abs(minorValue - Major[majorHintPointer].Value);

            int actual = majorHintPointer;
            while (++actual < Major.Count)
            {
                double actualValue = Math.Abs(Major[actual].Value - minorValue);
                if (actualValue < previousValue)
                    previousValue = actualValue;
                else
                    return actual - 1;
            }
            return actual - 1;
        }

        private int DecreaseLength(int minorPointer, int majorHintPointer)
        {
            double minorValue = Minor[minorPointer].Value;
            double previousValue = Math.Abs(minorValue - Major[majorHintPointer].Value);

            int actual = majorHintPointer;
            while (--actual >= 0)
            {
                double actualValue = Math.Abs(Major[actual].Value - minorValue);
                if (actualValue < previousValue)
                    previousValue = actualValue;
                else
                    return actual + 1;
            }
            return actual + 1;
        }


        private List<Vertex> GetVertexListFromIElements(List<IElement> elements)
        {
            List<Vertex> vertexList = new List<Vertex>(elements.Count);
            foreach (IElement e in elements)
                vertexList.Add(new Vertex(e));
            return vertexList;
        }
        #endregion
    }
}
