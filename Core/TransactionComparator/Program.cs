﻿using System;
using System.Collections.Generic;
using TransactionComparator.Interface;

namespace TransactionComparator
{
    class Program
    {
        static void Main(string[] args)
        {
            List<double> week1 = new List<double>() { -2029.00, -295.39, -193.00, -362.42, -17.00, -451.85, -4500.00, -150.25, -2836.50, -150.00, -357.60, -5000.00, -1.00, 5000.00, -155.60, -144.50, -87.50, -1249.00, -47.38, -189.00, -782.46, -20.00 };
            List<double> week2 = new List<double>() { -25.00, 0.00, -320.38, 0.00, -3.00, 17.37, 0.00, -76.17, -579.00, -5.00, -513.50, -74.50, 80.61, 25.00, -411.33, -380.00, -2276.27, 2276.27, -500.00 };

            List<double> income1 = new List<double>() { 5000.00};
            List<double> income2 = new List<double>() { 17.37, 80.61, 25.00, 2276.27 };

            List<double> expense1 = new List<double>() { -2029.00, -295.39, -193.00, -362.42, -17.00, -451.85, -4500.00, -150.25, -2836.50, -150.00, -357.60, -5000.00, -1.00, -155.60, -144.50, -87.50, -1249.00, -47.38, -189.00, -782.46, -20.00 };
            List<double> expense2 = new List<double>() { -25.00, -320.38, -3.00, -76.17, -579.00, -5.00, -513.50, -74.50, -411.33, -380.00, -2276.27, -500.00 };

            List<double> testA = new List<double>() { 1, 2, 7, 11, 11 };
            List<double> testB = new List<double>() { 1, 3, 8, 6 };

            var inputA = ConvertListToPackageList(expense1).ConvertAll<IElement>(e => e);
            var inputB = ConvertListToPackageList(expense2).ConvertAll<IElement>(e => e);

            Console.WriteLine("Create comparator:");
            Comparator cmp = new Comparator(inputA, inputB);

            Console.WriteLine("Compare:");
            cmp.Compare();

            Console.WriteLine("Write pairs:");
            cmp.WritePairs();

            Console.WriteLine("Write undetermined:");
            cmp.WriteUndetermined();

            Console.WriteLine("Done");
        }

        class Packager : IElement
        {
            public double Value { get; private set; }
            public bool Repetitive { get; set; }

            public Packager(double value)
            {
                Value = value;
            }   
        }

        private static List<Packager> ConvertListToPackageList(List<double> list)
        {
            List<Packager> returnList = new List<Packager>(list.Count);

            foreach (double e in list)
                returnList.Add(new Packager(e));

            return returnList;
        }
    }
}
