﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.BusinessLogic.ImportCSVLogic;
using Core.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = "RequireLoggedIn")]
    public class ImportController : ControllerBase
    {
        private readonly Model Context;
        private IModelImporter ModelImporter;

        public ImportController(Model context, IModelImporter modelImporter)
        {
            Context = context;
            ModelImporter = modelImporter;
        }

        // GET: api/Import
        [Authorize(Policy = "RequireLoggedIn")]
        public IActionResult Get()
        {
            Claim userJwtID = User.Claims.First(c => c.Type == "UserID");
            long userID = long.Parse(userJwtID.Value);

            var imports = Context.Files
                .Where(file => file.UserID == userID)
                .Select(f => new { id = f.ID, name = f.Name })
                .ToList();

            return Ok(new { count = imports.Count, imports });
        }

        // POST: api/Import
        [HttpPost]
        [Authorize(Policy = "RequireLoggedIn")]
        [RequestFormLimits(MultipartBodyLengthLimit = 268435456)]// Set the limit to 256 MB
        public async Task<IActionResult> Post()
        {
            if (Request == null || Request.Form == null || Request.Form.Files.Count < 1)
                return BadRequest("No file present");

            var files = Request.Form.Files
                .Where(f => Path.GetExtension(f.FileName).ToLower() == ".csv" && f.Length >= 10)
                .ToList();

            if (!files.Any())
                return BadRequest("Incorrect file format");

            Claim userJwtID = User.Claims.First(c => c.Type == "UserID");
            long userID = long.Parse(userJwtID.Value);

            IFormFile formFile = files.First();

            if (Context.Files.Any(f => f.Name == formFile.FileName && f.UserID == userID))
                return BadRequest("non-unique filename");

            string filePath = Path.GetTempFileName(); // full path to file in temp location

            int parsedCount = await SaveAndImport(formFile, filePath, userID); // process uploaded files

            if (parsedCount < 1)
                return BadRequest("File could not be parsed");

            return Ok(new { count = parsedCount, formFile.FileName });
        }

        private async Task<int> SaveAndImport(IFormFile formFile, string filePath, long userID)
        {
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await formFile.CopyToAsync(stream);
            }
            return ModelImporter.Import(filePath, formFile.FileName, userID);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Authorize(Policy = "RequireLoggedIn")]
        public IActionResult Delete(int id)
        {
            Claim userJwtID = User.Claims.First(c => c.Type == "UserID");
            long userID = long.Parse(userJwtID.Value);

            Models.File file = Context.Files
                .Include(f => f.Transactions)
                .FirstOrDefault(f => f.ID == id && f.UserID == userID);

            if (file == null)
                return Unauthorized(new { message = "file not found" });

            Context.Remove(file);
            Context.SaveChanges();

            return Ok();
        }
    }
}
