﻿using System;
using System.Linq;
using System.Security.Claims;
using Core.BusinessLogic.PasswordLogic;
using Core.Controllers.containers;
using Core.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[AllowAnonymous]
    public class RegistrationController : ControllerBase
    {
        private readonly Model Context;
        public RegistrationController(Model context)
        {
            Context = context;
        }

        // POST: api/Registration
        [HttpPost]
        public IActionResult Post([FromBody] CredentialContainer credentials)
        {
            Console.WriteLine($"username: {credentials.username}, password: {credentials.password}");

            //defensive 
            if (Context.Users.Any(u => u.Username == credentials.username))
                return BadRequest(new { credentials.username, status = 0, message = "Username already exist" });

            Context.Users.Add(new Models.User(credentials.username, credentials.password));
            Context.SaveChanges();
            return Ok(new { credentials.username, status = 1, message = "Registration Successful" });
        }

        // PUT: api/Registration
        [HttpPut]
        [Authorize(Policy = "RequireLoggedIn")]
        public IActionResult Put([FromBody] PasswordContainer passwordContainer)
        {
            Claim userJwtID = User.Claims.First(c => c.Type == "UserID");
            long userID = long.Parse(userJwtID.Value);

            Models.User user = Context.Users.FirstOrDefault(u => u.ID == userID);

            if(user==null)
                return BadRequest(new { status = 0, message = "user not found" });

            if(!PasswordHasher.Check(user.Password, passwordContainer.oldPassword))
                return BadRequest(new { status = 0, message = "wrong password" });


            user.Password = PasswordHasher.Hash(passwordContainer.newPassword);
            Context.Update(user);
            Context.SaveChanges();

            return Ok(new { status = 1, message = "Password change was successful" });
        }

        // Delete: api/Registration
        [HttpDelete]
        [Authorize(Policy = "RequireLoggedIn")]
        public IActionResult Delete([FromBody] PasswordDTO Password)
        {
            Claim userJwtID = User.Claims.First(c => c.Type == "UserID");
            long userID = long.Parse(userJwtID.Value);

            string password = Password.password;

            Models.User user = Context.Users
                .Include(u => u.Files)
                .ThenInclude(f => f.Transactions)
                .FirstOrDefault(u => u.ID == userID);

            if (user == null)
                return BadRequest(new { status = 0, message = "user not found" });

            if (!PasswordHasher.Check(user.Password, password))
                return BadRequest(new { status = 0, message = "wrong password" });

            Context.Remove(user);
            Context.SaveChanges();

            return Ok(new { status = 1, message = "Password change was successful" });
        }

        public class PasswordDTO
        {
            public string password { get; set; }
        }
    }
}
