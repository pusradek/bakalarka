﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.AI.NetworkLogic;
using Core.BusinessLogic.Predicators;
using Core.Controllers.containers;
using Core.Data;
using Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NeuralNetwork.NeuralNetNamespace;

namespace Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ForecastController : ControllerBase
    {
        private readonly Model Context;

        public ForecastController(Model context)
        {
            Context = context;
        }

        // GET: api/Forecast
        [HttpGet]
        [Authorize(Policy = "RequireLoggedIn")]
        public ForecastTransactionContainer Get()
        {
            Claim userJwtID = User.Claims.First(c => c.Type == "UserID");
            long userID = long.Parse(userJwtID.Value);

            var transactions = Context.Files
                .Where(f => f.UserID == userID)
                .SelectMany(f => f.Transactions)
                .Where(t => t.Amount < -2);

            if (!transactions.Any())
                return null;

            var averagePrediction = AveragePrediction.Predict(transactions);
            var amountPrediction = AmountPrediction.GetWeekComparsion(transactions);
            var neuralPrediction = GetNeuralNetworkPrediction(transactions, userID);
            return new ForecastTransactionContainer(averagePrediction, amountPrediction, neuralPrediction);
        }

        private List<TransactionContainer> GetNeuralNetworkPrediction(IQueryable<Transaction> transactions, long userID)
        {
            DateTime maxDate = transactions.Max(t => t.Date);
            DateTime lowerbound = maxDate.AddDays(-7);
            lowerbound = new DateTime(lowerbound.Year, lowerbound.Month, lowerbound.Day); //round to null hours and mins

            var intervalTransactions = transactions
                .Where(t => t.Date >= lowerbound);

            NeuralNet ann = GetNetwork(userID);
            Feeder input = new Feeder(Context, userID, lowerbound);

            List<TransactionContainer> expenses = new List<TransactionContainer>();
            while (input.GetNext())
            {
                if (!ann.EvaluateInput(input.Input))
                    continue;

                TransactionContainer container = new TransactionContainer()
                {
                    amount = input.NormalizationDataContainer.Amount,
                    name = input.NormalizationDataContainer.KindID != 0 ? Context.Kinds.First(k => k.ID == input.NormalizationDataContainer.KindID).Value  : "Platba bankovní kartou"
                };
                expenses.Add(container);
            }

            return expenses;
        }

        private NeuralNet GetNetwork(long userID)
        {
            long? networkID = Context.Users
                .FirstOrDefault(u => u.ID == userID)
                .NetworkID;

            var networksQuery = Context.Networks
                    .Include(net => net.Layer)
                    .ThenInclude(layer => layer.Neuron)
                    .ThenInclude(neuron => neuron.Weight);

            Network network = networkID == null ? networksQuery.First() : (networksQuery.First(n => n.ID == networkID) ?? networksQuery.First());

            NeuralNet ann = new NeuralNet();
            ann.Import(network.ToImportList());
            return ann;
        }
    }
}
