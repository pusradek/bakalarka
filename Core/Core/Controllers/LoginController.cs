﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Core.Controllers.containers;
using Core.Data;
using Core.Helpers;
using Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly AppSettings Settings;
        private readonly Model Context;

        public LoginController(Model context, IOptions<AppSettings> appSettings)
        {
            Context = context;
            Settings = appSettings.Value;
        }

        private SecurityTokenDescriptor CreateTokenDescriptor(string ID, string username)
        {
            // JWT token options
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Settings.Secret));
            double tokenExpires = Convert.ToDouble(Settings.ExpireTime);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim(JwtRegisteredClaimNames.Sub, username),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim("UserID", ID.ToString()),
                    new Claim("LoggedOn", DateTime.Now.ToString())
                }),
                SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature),
                Issuer = Settings.Site,
                Audience = Settings.Audience,
                Expires = DateTime.Now.AddMinutes(tokenExpires)
            };

            return tokenDescriptor;
        }

        // POST: api/Login
        [HttpPost]
        public IActionResult Post([FromBody] CredentialContainer credentials)
        {
            User user = Context.Users.FirstOrDefault(u => u.Username == credentials.username);

            if (user == null || !user.IsEqual(credentials.username, credentials.password))
            {
                ModelState.AddModelError("", "wrong password or user does not exist");
                return Unauthorized(new { LoginError = "Please Check the Login Credentials - Invalid username/password was entered" });
            }

            // generate JWT token
            var tokenDescriptor = CreateTokenDescriptor(user.ID.ToString(), credentials.username);
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return Ok(new { token = tokenHandler.WriteToken(token), expiration = token.ValidTo, username = user.Username });
        }
    }
}
