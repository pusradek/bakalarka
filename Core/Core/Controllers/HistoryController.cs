﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Controllers.containers;
using Core.Data;
using Core.Models;
using Core.Models.Codebooks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistoryController : ControllerBase
    {
        private readonly Model Context;

        public HistoryController(Model context)
        {
            Context = context;
        }

        // GET: api/History
        [HttpGet]
        [Authorize(Policy = "RequireLoggedIn")]
        public TransactionSumsContainer Get()
        {
            Claim userJwtID = User.Claims.First(c => c.Type == "UserID");
            long userID = long.Parse(userJwtID.Value);

            var transactions = Context.Files
                .Where(f => f.UserID == userID)
                .SelectMany(f => f.Transactions)
                .Where(t => t.Amount < -2);

            if (!transactions.Any())
                return null;

            return GetWeekSums(transactions);
        }

        private TransactionSumsContainer GetWeekSums(IQueryable<Transaction> transactions)
        {
            DateTime maxDate = transactions.Max(t => t.Date);

            //create weekly intervals for transaction
            DateTime lowerbound = maxDate.AddDays(-4 * 7);
            lowerbound = new DateTime(lowerbound.Year, lowerbound.Month, lowerbound.Day, 0, 0, 0);
            DateTime upperbound = maxDate.AddDays(-3 * 7);
            upperbound = new DateTime(upperbound.Year, upperbound.Month, upperbound.Day, 23, 59, 59);
            maxDate = new DateTime(maxDate.Year, maxDate.Month, maxDate.Day, 23, 59, 59);

            //assign category to transactions 4 week old
            IQueryable<Transaction> customInterval = transactions.Where(t => t.Amount < -2 && t.Date >= lowerbound);
            var symbolCategories = GetTransactionSymbolCategories(customInterval);
            var transactionSizes = GetTransactionTypes(customInterval);

            //group transactions by weeks
            List<TransactionContainer> expenses = new List<TransactionContainer>(4);
            while (upperbound <= maxDate)
            {
                IQueryable<Transaction> intervalTransactions = transactions
                    .Where(t => t.Amount < -2 && t.Date >= lowerbound && t.Date < upperbound);

                expenses.Add(new TransactionContainer()
                {
                    amount = (double)intervalTransactions.Sum(t => Math.Abs(t.Amount)),
                    count = intervalTransactions.Count()
                });

                lowerbound = lowerbound.AddDays(7);
                upperbound = upperbound.AddDays(7);
            }

            return new TransactionSumsContainer(expenses, symbolCategories.PaymentCategories, symbolCategories.PaymentTypes, transactionSizes);
        }

        private bool IsInInterval(int low, int high, decimal value) => value > low && value <= high;
        private TransactionContainer MakeTransactionContainer(IQueryable<Transaction> transactions, string name)
        {
            return new TransactionContainer()
            {
                amount = (double)transactions.Sum(t => Math.Abs(t.Amount)),
                count = transactions.Count(),
                name = name
            };
        }

        private List<TransactionContainer> GetTransactionTypes(IQueryable<Transaction> transactions)
        {
            return new List<TransactionContainer>()
            {
                MakeTransactionContainer( transactions.Where(t => IsInInterval(2,500,Math.Abs(t.Amount))),"Drobné - do 500 Kč"),
                MakeTransactionContainer( transactions.Where(t => IsInInterval(500,5000,Math.Abs(t.Amount))),"Malé - do 5 000 Kč"),
                MakeTransactionContainer( transactions.Where(t => IsInInterval(5000,25000,Math.Abs(t.Amount))),"Střední - do 25 000 Kč"),
                MakeTransactionContainer( transactions.Where(t => IsInInterval(25000,50000,Math.Abs(t.Amount))),"Velké - do 50 000 Kč"),
                MakeTransactionContainer( transactions.Where(t => IsInInterval(50000,500000,Math.Abs(t.Amount))),"Velmi velké - do 500 000 Kč"),
                MakeTransactionContainer( transactions.Where(t => Math.Abs(t.Amount)>500000),"Značně velké - nad 500 000 Kč")
            };
        }

        private List<TransactionContainer> MakeGroup(IQueryable<Tuple<double, AbstractConstantSymbolVariation>> transactions, double undeterminedAmount = 0, double undeterminedCount = 0)
        {
            const string labelUndefined = "Platba bankovní kartou";

            List<TransactionContainer> list = transactions.GroupBy(s => s.Item2)
                .Select(group => new TransactionContainer(
                    group.Sum(x => x.Item1),
                    group.Count(),
                    group.First().Item2 != null ? group.First().Item2.Value : labelUndefined
                )).ToList();

            if (undeterminedAmount == 0)
                return list;


            int index = list.FindIndex(t => t.name == labelUndefined);

            if (index == -1)
                list.Add(new TransactionContainer(undeterminedAmount, undeterminedCount, labelUndefined));
            else
                list[index] = new TransactionContainer(undeterminedAmount + list[index].amount, undeterminedCount + list[index].count, labelUndefined);

            return list;
        }

        private (List<TransactionContainer> PaymentTypes, List<TransactionContainer> PaymentCategories) GetTransactionSymbolCategories(IQueryable<Transaction> transactions)
        {
            var symbols = transactions
                .Where(t => t.ConstantSymbol != null)
                .Select(t => new { t.Amount, t.ConstantSymbol });

            //if reserved symbol = other symbols not possible
            var reserved = symbols.Where(t => t.ConstantSymbol.ReservedSymbol != null);
            var reservedSymbols = MakeGroup(reserved.Select(t => new Tuple<double, AbstractConstantSymbolVariation>((double)t.Amount, t.ConstantSymbol.ReservedSymbol)));


            decimal undeterminedAmount = transactions.Where(t => t.ConstantSymbol == null).Sum(t => t.Amount);
            int undeterminedCount = transactions.Where(t => t.ConstantSymbol == null).Count();
            symbols = symbols.Where(t => t.ConstantSymbol.ReservedSymbol == null);

            //var paymentTypes = MakeGroup(symbols.Select(t => new Tuple<double, AbstractConstantSymbolVariation>((double)t.Amount, t.ConstantSymbol.LastNumber)), (double)undeterminedAmount);
            var paymentTypes = MakeGroup(symbols.Select(t => new Tuple<double, AbstractConstantSymbolVariation>((double)t.Amount, t.ConstantSymbol.Kind)), (double)undeterminedAmount, undeterminedCount);
            var paymentCategories = MakeGroup(symbols.Select(t => new Tuple<double, AbstractConstantSymbolVariation>((double)t.Amount, t.ConstantSymbol.MinistryPredefined)), (double)undeterminedAmount, undeterminedCount);

            paymentCategories.AddRange(reservedSymbols);
            paymentTypes.AddRange(reservedSymbols);

            return (paymentTypes, paymentCategories);
        }
    }
}
