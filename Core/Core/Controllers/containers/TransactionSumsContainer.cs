﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Controllers.containers
{
    public class TransactionSumsContainer
    {
        public readonly List<TransactionContainer> expenses;

        //TransactionContainer.name = constant symbol
        //TransactionContainer.amount = amount per selcted constant symbol
        public readonly List<TransactionContainer> expensesPerSymbolCategory;
        public readonly List<TransactionContainer> expensesPerSymbolType;

        //TransactionContainer.name = type of transaction (small, big, very big)
        //TransactionContainer.amount = amount per type
        public readonly List<TransactionContainer> expensesPerSize;

        public TransactionSumsContainer(List<TransactionContainer> expenses, List<TransactionContainer> expensesPerSymbolCategory, List<TransactionContainer> expensesPerSymbolType, List<TransactionContainer> expensesPerSize)
        {
            this.expenses = expenses;
            this.expensesPerSymbolCategory = expensesPerSymbolCategory;
            this.expensesPerSymbolType = expensesPerSymbolType;
            this.expensesPerSize = expensesPerSize;
        }
    }
}
