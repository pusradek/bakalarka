﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Controllers.containers
{
    public class ForecastTransactionContainer
    {
        /// <summary>
        /// Mon - Sun numbered list
        /// each transactionContainer contains only amount and count
        /// </summary>
        public List<List<TransactionContainer>> averagePrediction;

        /// <summary>
        /// one week prediction with fields for each day
        /// each TransactionContainer contains only values: amount, kind (=name), repetitivness, sizeType
        /// </summary>
        public List<TransactionContainer> amountPredictionSizeTypes;
        public List<TransactionContainer> amountPredictionKinds;
        public List<TransactionContainer> amountPredictionList;

        /// <summary>
        /// neural net prediction for next 4 weeks
        /// contains amount, kind, sizeType
        /// </summary>
        public List<TransactionContainer> neuralPrediction;
        public List<TransactionContainer> neuralPredictionSizeTypes;

        public ForecastTransactionContainer(List<Tuple<double[], double[]>> averagePrediction, List<Tuple<double, string, bool>> amountPrediction, List<TransactionContainer> neuralPrediction)
        {
            //converts averagePrediction tuples to list of transactions
            this.averagePrediction = new List<List<TransactionContainer>>(averagePrediction.Count);
            for (int i = 0; i < averagePrediction.Count; i++)
            {
                this.averagePrediction.Add(new List<TransactionContainer>(7));

                for (int j = 0; j < 7; j++)
                    this.averagePrediction[i].Add(new TransactionContainer
                    {
                        count = averagePrediction[i].Item1[j],
                        amount = averagePrediction[i].Item2[j],
                    });
            }

            FillAmountPredictionLists(amountPrediction.Where(x => x.Item3).ToList());

            this.neuralPrediction = neuralPrediction;
            this.neuralPrediction.ForEach(c => c.sizeType = GetTransactionTypesName(Math.Abs(c.amount)));

            neuralPredictionSizeTypes = neuralPrediction
                .GroupBy(t => t.sizeType)
                .Select(g => new TransactionContainer()
                {
                    count = g.Count(),
                    amount = g.Sum(x => x.amount),
                    name = g.First().sizeType
                })
                .ToList();
        }

        private void FillAmountPredictionLists(List<Tuple<double, string, bool>> amountPrediction)
        {
            amountPredictionSizeTypes = MakeSizeTypesList(amountPrediction);

            amountPredictionList = amountPrediction
                .Select(x => new TransactionContainer()
                {
                    amount = x.Item1,
                    name = x.Item2
                }).ToList();

            amountPredictionKinds = amountPrediction
                .GroupBy(t => t.Item2)
                .Select(g => new TransactionContainer()
                {
                    count = g.Count(),
                    amount = g.Sum(x => x.Item1),
                    name = g.First().Item2
                })
                .ToList();
        }

        private List<TransactionContainer>  MakeSizeTypesList(List<Tuple<double, string, bool>> amountPrediction)
        {
            List<TransactionContainer> sizeTypesList = new List<TransactionContainer>()
            {
                new TransactionContainer(){ name="Drobné - do 500 Kč"},
                new TransactionContainer(){ name="Malé - do 5 000 Kč"},
                new TransactionContainer(){ name="Střední - do 25 000 Kč"},
                new TransactionContainer(){ name="Velké - do 50 000 Kč"},
                new TransactionContainer(){ name="Velmi velké - do 500 000 Kč"},
                new TransactionContainer(){ name="Značně velké - nad 500 000 Kč"}
            };

            foreach (var transaction in amountPrediction)
            {
                var targetType = sizeTypesList[GetTransactionTypesID(Math.Abs(transaction.Item1))];
                targetType.amount += transaction.Item1;
                targetType.count++;
            }

            return sizeTypesList;
        }

        private int GetTransactionTypesID(double amount)
        {
            if (amount < 500)
                return 0;
            if (amount < 5000)
                return 1;
            if (amount < 25000)
                return 2;
            if (amount < 50000)
                return 3;
            if (amount < 500000)
                return 4;

            return 5;
        }

        private string GetTransactionTypesName(double amount)
        {
            if (amount < 500)
                return "Drobné - do 500 Kč";
            if (amount < 5000)
                return "Malé - do 5 000 Kč";
            if (amount < 25000)
                return "Střední - do 25 000 Kč";
            if (amount < 50000)
                return "Velké - do 50 000 Kč";
            if (amount < 500000)
                return "Velmi velké - do 500 000 Kč";

            return "Značně velké - nad 500 000 Kč";
        }
    }
}
