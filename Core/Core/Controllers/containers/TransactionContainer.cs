﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Controllers.containers
{
    public class TransactionContainer
    {
        public double amount;
        public double count;
        public string name;
        public string sizeType;
        public bool repetitive;

        public TransactionContainer()
        {
        }

        public TransactionContainer(double amount, double count, string name)
        {
            this.amount = amount;
            this.count = count;
            this.name = name;
        }
    }
}
