﻿using Core.Data;
using Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;


namespace Core.AI.Categorization
{
    /// <summary>
    /// K-means algorithm
    /// </summary>
    /// <see cref="https://en.wikipedia.org/wiki/K-means_clustering"/>
    public class Categorization
    {
        private const int NUM_CATEGORIES = 7;

        private readonly Model data;
        private readonly NLog.Logger Logger;

        Thread t_income;
        Thread t_expenses;

        private readonly Category[] incomeCategories;
        private readonly Category[] expenseCategories;
        private List<Mean> income;
        private List<Mean> expenses;

        public Categorization(IServiceProvider services)
        {
            data = services.GetRequiredService<Model>();
            Logger = NLog.LogManager.GetCurrentClassLogger();

            incomeCategories = new Category[NUM_CATEGORIES];
            expenseCategories = new Category[NUM_CATEGORIES];
        }

        /// <summary>
        /// Sort data and initiate categories with approximate middle of the category
        /// approximate middle = middle of each segment of the (input) array
        /// </summary>
        /// <param name="means">data to categorize</param>
        /// <param name="categories">categories to initialize</param>
        private void InitCategories(List<Mean> means, Category[] categories)
        {
            Logger.Info("InitatingCategory (th. Name: " + Thread.CurrentThread.Name + ')');
            means.Sort();
            //center of segments
            int division = means.Capacity / (NUM_CATEGORIES + 1);

            for(int i = 1; i <= categories.Length; i++)
            {
                int position = i * division;
                categories[i-1] = new Category(i, means[position], position);
            }
        }

        /// <summary>
        /// find closest category for some given mean
        /// </summary>
        /// <remarks>Possible otimalization - vinary search in categories</remarks>
        /// <param name="mean">search key</param>
        /// <param name="categories">available categories</param>
        /// <returns>minimal category</returns>
        Category GetNearestCategory (Mean mean, Category[] categories)
        {
            Category minCategory = categories[0];
            double minDistance = Math.Abs(minCategory.CentroidValue - mean.amount);

            foreach (Category category in categories)
            {
                double currentDistance = Math.Abs(category.CentroidValue - mean.amount);
                if (currentDistance >= minDistance)
                    continue;

                minDistance = currentDistance;
                minCategory = category;
            }
            return minCategory;
        }

        /// <summary>
        /// divide categories by input elements
        /// - each iteration increase accuracy
        /// - stops when no elements is changing anymore
        /// </summary>
        /// <param name="means">input (categorization) data (</param>
        /// <param name="categories">categories with coresponding elements</param>
        private void LearnCategories(List<Mean> means, Category[] categories)
        {
            Logger.Info("Learning Category (th. Name: " + Thread.CurrentThread.Name + ')');
            foreach (Category category in categories)
                category.Clear();

            foreach (Mean mean in means)
                GetNearestCategory(mean, categories).AddMember(mean);

            bool categoryChanged = false;

            foreach (Category category in categories)
            {
                category.CheckCenter(means);
                if (category.Changed)
                    categoryChanged = true;
            }

            if (categoryChanged)
                LearnCategories(means, categories);
        }

        /// <summary>
        /// workflow for income learning
        /// </summary>
        private void LearnIncome()
        {
            InitCategories(income, incomeCategories);
            LearnCategories(income, incomeCategories);
        }

        /// <summary>
        /// workflow for expenses learning
        /// </summary>
        private void LearnExpenses()
        {
            InitCategories(expenses, expenseCategories);
            LearnCategories(expenses, expenseCategories);
        }

        /// <summary>
        /// starts 2 additional threads and wait for completion
        /// </summary>
        public void Compute()
        {
            GetAllPricesFromDatabase();

            t_income = new Thread(LearnIncome);
            t_income.Name = "t_income";
            t_expenses = new Thread(LearnExpenses);
            t_expenses.Name = "t_expenses";
            t_income.Start();
            t_expenses.Start();


            t_income.Join();
            Logger.Info("Categorization of Income has finished");
            String output = "============= Categorization Income Completed ================\n";
            foreach (Category category in incomeCategories)
                output += "===" + category.ToString();
            Console.WriteLine(output);

            t_expenses.Join();
            Logger.Info("Categorization of Expenses has finished");
            output = "============= Categorization Expense Completed ================\n";
            foreach (Category category in expenseCategories)
                output += "===" + category.ToString();
            Console.WriteLine(output);
        }

        /// <summary>
        /// get data from database (income + expenses)
        /// </summary>
        private void GetAllPricesFromDatabase()
        {
            //set some initial size for list (1/3 + 2/3)
            int count = data.Transactions.Count();
            count /= 3;
            expenses = new List<Mean>(count);
            income = new List<Mean>(count * 2);


            foreach (Transaction t in data.Transactions.Include(t => t.ConstantSymbol))
            {
                if (/*t.Repetition != null ||*/ Math.Abs(t.Amount) < 20) continue;
                //if (t.Repetition != null || Math.Abs(t.Amount) < 1 || t.ConstantSymbol==null || t.ConstantSymbol.ProvidedID!=1178) continue;

                if (t.Amount > 0)
                    income.Add(new Mean(t.Amount));
                else
                    expenses.Add(new Mean(-t.Amount));
            }

            income.TrimExcess();
            expenses.TrimExcess();
        }
    }
}
