﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.AI.Categorization
{
    public class Category
    {
        public readonly int ID;
        public long CentroidPosition { get; internal set; }
        public double CentroidValue { get; internal set; }
        public bool Changed { get; internal set; }

        //distance
        private double min;
        private double max;
        private double average;

        public Category(int ID, Mean mean, long position)
        {
            this.ID = ID;
            min = max = CentroidValue = mean.amount;
            average = -1;
            CentroidPosition = position;
        }

        /// <summary>
        /// clear min, max values
        /// </summary>
        public void Clear()
        {
            min = max = CentroidValue;
        }

        /// <summary>
        /// add data about category member
        /// </summary>
        /// <param name="mean"></param>
        public void AddMember(Mean mean)
        {
            double currentAmount = mean.amount;
            if (min > currentAmount)
                min = currentAmount;
            else
                if (max < currentAmount)
                max = currentAmount;

        }

        /// <summary>
        /// set current center
        /// </summary>
        /// <param name="means"></param>
        internal void CheckCenter(List<Mean> means)
        {
            double average = (min + max) / 2.0;
            if (this.average == average)
            {
                Changed = false;
                return;
            }

            int start = means.BinarySearch(new Mean(min));
            int end = means.BinarySearch(new Mean(max));

            double currentBest = Math.Abs(min - average);
            int currentBestPosition = start;

            for (int i = start; i != end; i++)
            {
                double cycleDistance = Math.Abs(means[i].amount - average);

                if (cycleDistance >= currentBest)
                    continue;

                currentBest = cycleDistance;
                currentBestPosition = i;
            }

            if (currentBestPosition == CentroidPosition)
            {
                Changed = false;
                return;
            }

            CentroidPosition = currentBestPosition;
            CentroidValue = means[currentBestPosition].amount;
            this.average = average;
            Changed = true;
        }

        public override string ToString()
        {
            string str = "Category ID: " + ID + '\n';
            str += "CentroidPosition: " + CentroidPosition + '\n';
            str += "CentroidValue: " + CentroidValue + '\n';
            str += "min: " + min + '\n';
            str += "max: " + max + '\n';
            str += "average: " + average + '\n';

            return str;
        }
    }
}
