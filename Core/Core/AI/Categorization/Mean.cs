﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.AI.Categorization
{
    /// <summary>
    /// data container
    /// </summary>
    public class Mean : IComparable
    {
        public readonly double amount;
        public byte category;

        public Mean(double amount)
        {
            this.amount = amount;
        }

        public Mean(decimal amount) : this((double)amount)
        {
        }

        #region operator definitions
        public override bool Equals(object obj)
        {
            return obj is Mean mean &&
                   amount == mean.amount;
        }

        public static bool operator ==(Mean left, Mean right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Mean left, Mean right)
        {
            return !(left == right);
        }

        public static bool operator <(Mean left, Mean right)
        {
            return left.amount < right.amount;
        }

        public static bool operator >(Mean left, Mean right)
        {
            return left.amount > right.amount;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public int CompareTo(object obj)
        {
            if (!(obj is Mean))
                throw new ArgumentException("Unsupported type");

            Mean right = (Mean)obj;

            if (right == this) return 0;
            return right > this ? -1 : 1;
        }
        #endregion
    }
}
