﻿using Core.Data;
using Core.Models;
using Microsoft.EntityFrameworkCore;
using NeuralNetwork.NeuralNetNamespace.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransactionComparator;
using TransactionComparator.Interface;

namespace Core.AI.NetworkLogic
{
    public class Feeder : IInput
    {
        #region interface implementation
        public List<double> Input { get; private set; }

        public bool? Result { get; private set; }

        public bool SeekEnd { get; private set; }
        #endregion

        private readonly Model Context;
        private readonly long UserID;
        private DateTime LowerBoundDate;
        List<NormalizationContainer> DataSet;

        public int MaxBatchIteration = 20;
        public NormalizationContainer NormalizationDataContainer { get; private set; }
        private int BatchIteration = 0;
        private int ListIteration = -1;

        // seek / comparsion interval
        private const int DAYSTEP = 14;

        public Feeder(Model context, long userID)
        {
            Context = context;
            UserID = userID;

            DateTime lowerBoundDate = Context.Files
                .Where(f => f.UserID == UserID)
                .SelectMany(f => f.Transactions)
                .Min(t => t.Date);
        }

        public Feeder(Model context, long userID, DateTime lowerBoundDate)
        {
            Context = context;
            UserID = userID;
            LowerBoundDate = lowerBoundDate;
        }



        public bool GetNext()
        {
            ListIteration++;

            if (BatchIteration > MaxBatchIteration)
                return false;

            if (SeekEnd || DataSet == null || ListIteration >= DataSet.Count)
            {
                if (++BatchIteration > MaxBatchIteration)
                    return false;

                DataSet = GetDataset();

                ListIteration = 0;
            }
            SeekEnd = false;

            if (DataSet == null || DataSet.Count == 0)
            {
                Console.WriteLine($"Batch iteration: {BatchIteration}");
                return false;
            }


            NormalizationDataContainer = DataSet[ListIteration];

            SeekEnd = NormalizationDataContainer.SeekEnd = ListIteration >= DataSet.Count - 1;
            Result = NormalizationDataContainer.Repetitive;
            Input = NormalizationDataContainer.Input;

            return true;
        }

        /// <summary>
        /// get TransactionSet for later iteration
        /// </summary>
        /// <returns>iterable TransactionSet</returns>
        private List<NormalizationContainer> GetDataset()
        {
            List<NormalizationContainer> firstChunk = GetChunkFromDB(LowerBoundDate, LowerBoundDate.AddDays(DAYSTEP));
            LowerBoundDate = LowerBoundDate.AddDays(DAYSTEP);
            List<NormalizationContainer> secondChunk = GetChunkFromDB(LowerBoundDate, LowerBoundDate.AddDays(DAYSTEP));
            //LowerBoundDate = LowerBoundDate.AddDays(DAYSTEP);

            Comparator comparator = new Comparator(firstChunk.ConvertAll(e => (IElement)e), secondChunk.ConvertAll(e => (IElement)e));
            comparator.Compare();
            //return comparator.GetRepetitivness().Item1.ConvertAll(e => (NormalizationContainer)e);
            var compared = comparator.GetRepetitivness();
            List<IElement> list = compared.Item1;
            list.AddRange(compared.Item2);
            return list.ConvertAll(e => (NormalizationContainer)e);
        }

        /// <summary>
        /// get transactions within <mindate;maxdate) interval of times
        /// </summary>
        /// <param name="minDate">all transaction from this date (including)</param>
        /// <param name="maxDate">all transaction to this date (without)</param>
        /// <returns></returns>
        private List<NormalizationContainer> GetChunkFromDB(DateTime minDate, DateTime maxDate)
        {
            IQueryable<Transaction> transactions = Context.Files
                .Where(f => f.UserID == UserID)
                .SelectMany(f => f.Transactions)
                .Include(t => t.SenderAccount)
                .Include(t => t.ConstantSymbol)
                .Where(t => t.Date >= minDate && t.Date < maxDate);

            List<NormalizationContainer> containers = new List<NormalizationContainer>(transactions.Count());

            foreach (Transaction transaction in transactions)
            {
                if (Exclusions(transaction))
                    continue;
                containers.Add(new NormalizationContainer(transaction));
            }


            return containers;
        }

        private bool Exclusions(Transaction transaction)
        {
            //incoming transaction / fees
            return transaction.Amount > -2;
        }
    }
}
