﻿using Core.Data;
using NeuralNetwork.NeuralNetNamespace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.AI.NetworkLogic
{
    public class AITester
    {
        private const long CONSTUSERID = 1;
        private const int inputSize = 80;
        private readonly Model Context;

        private NeuralNet Ann;
        public AITester(Model model)
        {
            Context = model;
        }

        public void Test()
        {
            Ann = CreateTrainedNet();
            Evaluate(Ann);
            MakeConsoleCommand();
            return;
        }



        private void MakeConsoleCommand()
        {
            Console.WriteLine("R: recreate net (default), e: exit, s: saveToDB");
            string userInput = Console.ReadLine().ToLower();
            switch (userInput)
            {
                case "":
                case "r"://recreate
                    {
                        Ann = CreateTrainedNet();
                        Evaluate(Ann);
                        MakeConsoleCommand();
                        return;
                    }

                case "e": return;//exit

                case "s"://save
                    {
                        var export = Ann.ExportNet();
                        Context.Networks.Add(new Models.Network(export));
                        Context.SaveChanges();
                        Console.WriteLine("NetworkSaved.");
                        MakeConsoleCommand();
                        return;
                    }

                default:
                    {
                        Console.WriteLine("Unknown option. Please try again");
                        MakeConsoleCommand();
                        return;
                    }
            }
        }

        private NeuralNet CreateTrainedNet()
        {
            NeuralNet ann = new NeuralNet();
            ann.CreateEmpty(new List<int> { inputSize, inputSize, (int)(1.5 * inputSize), inputSize, inputSize / 2, inputSize / 4, inputSize / 8, 1 });
            //ann.CreateEmpty(new List<int> { 2, 4, 4, 2, 1 });

            Feeder input = new Feeder(Context, CONSTUSERID);
            input.MaxBatchIteration = 6;
            ann.BatchInput(input);
            return ann;
        }

        private void Evaluate(NeuralNet ann)
        {
            Feeder input = new Feeder(Context, CONSTUSERID);
            input.MaxBatchIteration = 1;

            int success = 0;
            int fail = 0;
            int resultYes = 0;
            int resultNo = 0;
            int predictionsYes = 0;
            int predictionsNo = 0;
            while (input.GetNext())
            {
                bool prediction = ann.EvaluateInput(input.Input);

                if (prediction)
                    predictionsYes++;
                else
                    predictionsNo++;

                if (prediction == input.Result)
                    success++;
                else
                    fail++;

                if ((bool)input.Result)
                    resultYes++;
                else
                    resultNo++;
                //Console.WriteLine(ann.ToString());//DEBUG string
            }
            Console.WriteLine($"success: {success}, fail: {fail} => {(success / (double)(success + fail)) * 100}%");
            Console.WriteLine($"resultYes: {resultYes}, resultNo: {resultNo}");
            Console.WriteLine($"predictionsYes: {predictionsYes}, predictionsNo: {predictionsNo}");
        }
    }
}
