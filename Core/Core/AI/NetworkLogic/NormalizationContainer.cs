﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Models;
using NeuralNetwork.NeuralNetNamespace.BusinessLogic;
using TransactionComparator.Interface;

namespace Core.AI.NetworkLogic
{
    public class NormalizationContainer : IElement
    {
        #region interface getters/setters

        #region IElement (TransactionComparator)
        public double Value { get => Amount; }
        public bool Repetitive { get; set; }
        #endregion

        #region IInput (Neural Network)
        public List<double> Input => GetAllValues();
        public bool? Result => Repetitive;
        public bool SeekEnd { get; set; }
        #endregion

        #endregion

        #region container values
        public double Amount { get; private set; }
        public DateTime Date { get; private set; }
        public int KindID { get; private set; }
        public int LastNumberID { get; private set; }
        public int ReservedSymbolID { get; private set; }
        public int MinistryPredefinedID { get; private set; }

        public List<double> GetAllValues()
        {
            List<double> list = new List<double>(80)
            {
                NormalizeAmount(Amount)
        };
            list.AddRange(DayToList());

            list.AddRange(RangeWithPositionValue(6, ReservedSymbolID));
            list.AddRange(RangeWithPositionValue(10, KindID));
            list.AddRange(RangeWithPositionValue(10, LastNumberID));
            list.AddRange(RangeWithPositionValue(42, MinistryPredefinedID));

            return list;
        }

        /// <summary>
        /// Creates list of 7 values. Each has value -1 but the target day, which has value of 1;
        /// </summary>
        /// <returns>list with marked current day as 1</returns>
        private List<double> DayToList()
        {
            switch (Date.DayOfWeek)
            {
                case DayOfWeek.Monday: return new List<double> { 1, -1, -1, -1, -1, -1, -1 };
                case DayOfWeek.Tuesday: return new List<double> { -1, 1, -1, -1, -1, -1, -1 };
                case DayOfWeek.Wednesday: return new List<double> { -1, -1, 1, -1, -1, -1, -1 };
                case DayOfWeek.Thursday: return new List<double> { -1, -1, -1, 1, -1, -1, -1 };
                case DayOfWeek.Friday: return new List<double> { -1, -1, -1, -1, 1, -1, -1 };
                case DayOfWeek.Saturday: return new List<double> { -1, -1, -1, -1, -1, 1, -1 };
                case DayOfWeek.Sunday: return new List<double> { -1, -1, -1, -1, -1, -1, 1 };
                default: throw new ArgumentException("Unexpected Day of Week");
            }
        }

        private List<double> RangeWithPositionValue(int range, int position)
        {
            range++;//increase for default value at '0' position

            List<double> resultList = new List<double>(range);
            for (int i = 0; i < range; i++)
                resultList.Add(-1);

            resultList[position] = 1;

            return resultList;
        }

        #endregion

        public NormalizationContainer(Transaction transaction)
        {
            Amount = (double)transaction.Amount;
            Date = transaction.Date;

            ReservedSymbolID = (int)(transaction.ConstantSymbol?.ReservedSymbolID ?? 0);
            if (ReservedSymbolID != 0)
            {
                KindID = LastNumberID = MinistryPredefinedID = 0;
                return;
            }

            KindID = (int)(transaction.ConstantSymbol?.KindID ?? 0);
            LastNumberID = (int)(transaction.ConstantSymbol?.LastNumberID ?? 0);
            MinistryPredefinedID = (int)(transaction.ConstantSymbol?.MinistryPredefinedID ?? 0);

        }

        private double NormalizeAmount(double input)
        {
            if (input < 500)
                return Math.Tanh(input / 189.0) / 3.0 - 1;

            if (input < 5000)
                return Math.Tanh(((input - 500) / 1889.0) + 1) / 3.0 - 1;

            if (input < 25000)
                return Math.Tanh(((input - 5000) / 9446.0) + 2) / 3.0 - 1;

            if (input < 50000)
                return Math.Tanh((input - 25000) / 18892.0) / 3.0;

            if (input < 500000)
                return Math.Tanh(((input - 50000) / 188918.0) + 1) / 3.0;

            return Math.Tanh(((input - 500000) / 1889180.0) + 2) / 3.0;
        }

    }
}
