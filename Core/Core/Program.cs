using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Core.AI.Categorization;
using Core.AI.NetworkLogic;
using Core.BusinessLogic.ImportCSVLogic;
using Core.BusinessLogic.TransactionContextOperations;
using Core.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Core
{
    public class Program
    {
        public static void Main(string[] args)
        {
            const bool initDB = false; //initialization of database
            const bool rebuildDB = false; // drop database; executes only if initDB = true

            const bool runClientApp = true;//run ClientApp
            const bool computeCategories = false; //K-means
            const bool trainAI = false;
            const bool testCSV = false;
            const bool fillConstantSymbols = false;

            IWebHost host = CreateWebHostBuilder(args).Build();
            IServiceProvider services = host.Services.CreateScope().ServiceProvider;

#pragma warning disable CS0162 // Unreachable code detected

            if (testCSV)
            {
                ImportTester tester = new ImportTester(services.GetRequiredService<Model>());
                tester.Test();
            }

            if (trainAI)
            {
                AITester tester = new AITester(services.GetRequiredService<Model>());
                tester.Test();
            }

            if (computeCategories)
                (new Categorization(services)).Compute();

            if (initDB)
                InitDatabase(services, rebuildDB);

            if (fillConstantSymbols)
                new ConstantSymbolOperations(services.GetRequiredService<Model>()).FillAll();

            if(runClientApp)
                host.Run();
#pragma warning restore CS0162 // Unreachable code detected
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
            => WebHost.CreateDefaultBuilder(args)
            .ConfigureLogging(logging =>
            {
                logging.AddFilter("Microsoft.EntityFrameworkCore.Database.Command", LogLevel.Warning);
                logging.AddFilter("Microsoft.AspNetCore.Hosting.Internal", LogLevel.Warning);
            })
            .UseStartup<Startup>();

        private static void InitDatabase(IServiceProvider services, bool rebuildDB=false)
        {
            try
            {
                Model context = services.GetRequiredService<Model>();
                DbInitializer.Initialize(context, rebuildDB);
            }
            catch (Exception exception)
            {
                ILogger logger = services.GetRequiredService<ILogger<Program>>();
                logger.LogError(exception, "An error ocurred while seeding the database");
                throw;
            }
        }
    }
}
