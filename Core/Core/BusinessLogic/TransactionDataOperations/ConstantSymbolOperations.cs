﻿using Core.Data;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.BusinessLogic.TransactionContextOperations
{
    public class ConstantSymbolOperations
    {
        private readonly Model Context;

        public ConstantSymbolOperations(Model context)
        {
            Context = context;
        }

        // https://cs.wikipedia.org/wiki/Konstantn%C3%AD_symbol_(pen%C4%9B%C5%BEn%C3%AD_p%C5%99evod)
        public void FillAll()
        {
            foreach (ConstantSymbol symbol in Context.ConstantSymbols)
            {
                symbol.ValidateThis();
                if (symbol.Valid)
                    DomesticInitialization(symbol);
            }
            Context.SaveChanges();
        }

        public ConstantSymbol Fill(ConstantSymbol symbol)
        {
            symbol.ValidateThis();
            if (symbol.Valid)
                DomesticInitialization(symbol);
            return symbol;
        }

        private void DomesticInitialization(ConstantSymbol cs)
        {
            int parseID = (int)cs.ProvidedID;

            //cca 6 numbers
            cs.ReservedSymbol = Context.ReservedSymbols.FirstOrDefault(x => x.Code == parseID);
            if (cs.ReservedSymbol != null)
                return;

            //numbers 0-9
            cs.LastNumber = Context.LastNumbers.FirstOrDefault(x => x.Code == parseID % 10);

            parseID /= 10;
            cs.Kind = Context.Kinds.FirstOrDefault(x => x.Code == parseID % 10);

            // 3-4 ciphers
            cs.MinistryPredefined = Context.MinistryPredefined.FirstOrDefault(x => (x.Code == parseID || x.Code == (int)cs.ProvidedID));
            cs.Valid = (cs.MinistryPredefined != null);

            return;
        }
    }
}
