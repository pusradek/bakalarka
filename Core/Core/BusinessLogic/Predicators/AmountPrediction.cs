﻿using Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransactionComparator;
using TransactionComparator.Interface;

namespace Core.BusinessLogic.Predicators
{
    public class AmountPrediction
    {
        public static List<Tuple<double, string, bool>> GetWeekComparsion(IQueryable<Transaction> transactions)
        {
            DateTime maxDate = transactions.Max(t => t.Date);

            DateTime upperBound = maxDate.AddDays(-(int)maxDate.DayOfWeek); //get last full week
            upperBound = new DateTime(maxDate.Year, maxDate.Month, maxDate.Day); // delete hours, minutes, miliseconds,...
            upperBound.AddDays(1); // strictly less than this value

            DateTime lowerBound = maxDate.AddDays(-7);

            var week1 = transactions
                .Include(t => t.ConstantSymbol).ThenInclude(s => s.Kind)
                .Where(t => t.Date >= lowerBound && t.Date<upperBound)
                .Select( t => new ComparatorHelperContainer(t)).ToList();

            lowerBound = lowerBound.AddDays(-7);
            upperBound = upperBound.AddDays(-7);
            var week2 = transactions
                .Include(t => t.ConstantSymbol).ThenInclude(s => s.Kind)
                .Where(t => t.Date >= lowerBound && t.Date < upperBound)
                .Select(t => new ComparatorHelperContainer(t)).ToList();

            Comparator comparator = new Comparator(week1.ConvertAll(x => (IElement) x), week2.ConvertAll(x => (IElement)x));
            comparator.Compare();

            return week1.ConvertAll(x => new Tuple<double, string, bool>(x.Value, x.Kind, x.Repetitive));
        }
    }

    internal class ComparatorHelperContainer : IElement
    {
        public double Value { get; }
        public bool Repetitive { get; set; }

        public readonly string Kind;

        public ComparatorHelperContainer(Transaction transaction)
        {
            Value = (double) transaction.Amount;
            Kind = transaction?.ConstantSymbol?.Kind?.Value ?? "Platba bankovní kartou";
        }
    }
}
