﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.BusinessLogic.Predicators
{
    public static class AveragePrediction
    {
        private const int ConstWeekRange = 4;

        /// <summary>
        /// Get per day count & average expenses from transactions by given week number
        /// </summary>
        /// <param name="weekNumber">week number numbered from 0</param>
        /// <returns>Mon - Sun numbered arrays in tuple (first: count, second: average)</returns>
        private static Tuple<double[], double[]> GetWeekCountAndAverage(IQueryable<Transaction> transactions, int weekNumber)
        {
            var weeks = transactions.Where(t => (t.Date.DayOfYear / 7) % ConstWeekRange == weekNumber);

            double[] weekCounts = new double[7];
            double[] weekAverage = new double[7];

            for (int i = 0; i < 7; i++)
            {
                var weekDayIntervals = weeks
                    .Select(t => new { t.Date, t.Amount }) //lesser data
                    .Where(t => (int)t.Date.DayOfWeek == i)
                    .ToList() //EF bug workaround
                    .GroupBy(t => new { t.Date.DayOfYear, t.Date.Year })
                    .Select(group => new
                     {
                         count = group.Count(),
                         amount = group.Average(t => t.Amount)
                     });

                if (weekDayIntervals.Any())
                {
                    weekCounts[i] = weekDayIntervals.Average(g => g.count);
                    weekAverage[i] = (double)weekDayIntervals.Average(g => g.amount);
                }
                else
                    weekCounts[i] = weekAverage[i] = 0;
            }


            //Sunday is defined as 0 / first day => make Monday first
            double countSwap = weekCounts[0];
            weekCounts[0] = weekCounts[6];
            weekCounts[6] = countSwap;

            double averageSwap = weekAverage[0];
            weekAverage[0] = weekAverage[6];
            weekAverage[6] = averageSwap;

            return new Tuple<double[], double[]>(weekCounts, weekAverage);
        }

        public static List<Tuple<double[], double[]>> Predict(IQueryable<Transaction> transactions)
        {
            List<Tuple<double[], double[]>> weeksPrediction = new List<Tuple<double[], double[]>>(ConstWeekRange);

            for (int i = 0; i < ConstWeekRange; i++)
                weeksPrediction.Add(GetWeekCountAndAverage(transactions, i));

            return weeksPrediction;
        }
    }
}
