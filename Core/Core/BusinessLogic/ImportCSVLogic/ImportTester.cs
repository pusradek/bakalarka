﻿using Core.Data;
using Core.Models;
using CsvHelper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Core.BusinessLogic.ImportCSVLogic
{
    public class ImportTester
    {
        private readonly Model model;

        public ImportTester(Model model)
        {
            this.model = model;
        }

        public void Test()
        {
            using (var reader = new StreamReader("C:\\Users\\Krypton0\\Desktop\\data-1565252156518.csv"))//csas
            using (var csv = new CsvReader(reader))
            {
                csv.Configuration.BadDataFound = null;
                csv.Configuration.HeaderValidated = null;
                csv.Configuration.Delimiter = ",";

                // Log missing field.
                csv.Configuration.MissingFieldFound = (headerNames, index, context) =>
                {
                    Console.WriteLine($"Field with names ['{string.Join("', '", headerNames)}'] at index '{index}' was not found.");
                };

                ModelImporter importer = new ModelImporter(model);
                importer.SaveRecords(1,"testName" + DateTime.Now.ToString() + ".csv", csv.GetRecords<DocumentLayout>());
            }
        }
    }
}
