﻿using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.BusinessLogic.ImportCSVLogic
{
    public class DocumentLayout
    {
        [Name("2")]
        public string AccountType { get; set; }
        [Name("číslo účtu")]
        public string Account { get; set; }
        [Name(new string[] { "datum zaúčtování", "datum provedení" })]
        public string Date { get; set; }
        [Name(new string[] { "částka", "částka v měně účtu" })]
        public string Amount { get; set; }
        [Name(new string[] { "měna", "původní měna úhrady" })]
        public string Currency { get; set; }
        [Name("zůstatek")]
        public string SenderAccountBallance { get; set; }
        [Name(new string[] { "číslo účtu protiúčtu", "protiúčet", "číslo účtu protistrany" })]
        public string RecipientAccount { get; set; }
        [Name(new string[] { "kód banky protiúčtu", "bankovní kód protiúčtu" })]
        public string RecipientBankCode { get; set; }
        [Name(new string[] { "název účtu protiúčtu", "název protiúčtu","název účtu protistrany" })]
        public string RecipientAccountName { get; set; }
        [Name("konstantní symbol")]
        public string ConstantSymbol { get; set; }
        [Name("variabilní symbol")]
        public string VariableSymbol { get; set; }
        [Name("specifický symbol")]
        public string SpecificSymbol { get; set; }
        [Name(new string[] { "označení operace", "typ transakce", "typ úhrady" })]
        public string TransactionType { get; set; }
        [Name("id transakce")]
        public string ID { get; set; }
        [Name("poznámka", "poznámka k úhradě")]
        public string Note { get; set; }

        public string Dump(string delimiter = "\n")
        {
            string str;

            str = AccountType + delimiter;
            str += Account + delimiter;
            str += Date.ToString() + delimiter;
            str += Amount + delimiter;
            str += Currency + delimiter;
            str += SenderAccountBallance + delimiter;
            str += RecipientAccount + delimiter;
            str += RecipientBankCode + delimiter;
            str += RecipientAccountName + delimiter;
            str += ConstantSymbol + delimiter;
            str += VariableSymbol + delimiter;
            str += SpecificSymbol + delimiter;
            str += TransactionType + delimiter;
            str += ID + delimiter;
            str += Note;

            return str;
        }
    }
}
