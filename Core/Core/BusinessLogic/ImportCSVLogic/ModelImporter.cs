﻿using Core.BusinessLogic.TransactionContextOperations;
using Core.Data;
using Core.Models;
using CsvHelper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Core.BusinessLogic.ImportCSVLogic
{
    public class ModelImporter : IModelImporter
    {
        private readonly Model model;
        private readonly ConstantSymbolOperations ConstantSymbolOperations;

        public ModelImporter(Model model)
        {
            this.model = model;
            ConstantSymbolOperations = new ConstantSymbolOperations(model);
        }

        public int Import(string filePath, string fileName, long userID)
        {
            string delimiter = GetDelimiter(filePath); 
            if (string.IsNullOrEmpty(delimiter))
                return -1;

            using (var reader = new StreamReader(filePath))
            using (var csv = MakeReader(reader,delimiter))
            {
                ModelImporter importer = new ModelImporter(model);
                return importer.SaveRecords(userID, fileName, csv.GetRecords<DocumentLayout>());
            }
        }

        #region csvReader settings
        private string GetDelimiter(string filePath)
        {
            string[] delimiters = new string[] { ",", ";" };
            foreach (string delimiter in delimiters)
            {
                using (var reader = new StreamReader(filePath))
                using (var csv = MakeReader(reader,delimiter))
                {
                    csv.Read();
                    csv.ReadHeader();
                    if (csv.Context.HeaderRecord.Count() > 3)
                        return delimiter;
                }
            }

            return "";
        }

        private CsvReader MakeReader(StreamReader stream, string delimiter)
        {
            var csvReader = new CsvReader(stream);
            csvReader.Configuration.BadDataFound = null;
            csvReader.Configuration.HeaderValidated = null;
            csvReader.Configuration.Delimiter = delimiter;
            csvReader.Configuration.PrepareHeaderForMatch = (string header, int index) => header.ToLower();
            csvReader.Configuration.MissingFieldFound = (headerNames, index, context) =>
            {
                Console.WriteLine($"Field with names ['{string.Join("', '", headerNames)}'] at index '{index}' was not found.");
            };

            return csvReader;
        }
        #endregion

        internal int SaveRecords(long userID, string fileName, IEnumerable<DocumentLayout> records)
        {
            User user = model.Users
                .Include(u => u.Files)
                .First(u => u.ID == userID);

            if (user == null)
                throw new ArgumentException($"UnknownUser with ID {userID}");

            if (user.Files.Any(f => f.Name == fileName))
                throw new ArgumentException($"Filename {fileName} already exists");

            try
            {
                if (!records.Any()) //throw reading exception for unsupported data
                    throw new ArgumentException("file not supported");
            }
            catch (Exception) { return -1; } //no members are mapped exception


            Models.File file = new Models.File() { Name = fileName, Transactions = new List<Transaction>() };

            string DefaultUserAccount = userID.ToString() + "/857368"; //857368 = UID in ASCII
            var transactionTypesList = model.TransactionTypes.ToList(); //DB performance
            var constantSymbolsList = model.ConstantSymbols.ToList();
            var accountList = model.Accounts.ToList();

            int succesfullyParsedLineCount = 0;
            try
            {
                foreach (var record in records)
                {
                    try
                    {
                        Transaction transaction = new Transaction(record);

                        if (!string.IsNullOrEmpty(record.TransactionType))
                            transaction.TransactionType = transactionTypesList.FirstOrDefault(t => record.TransactionType.StartsWith(t.Description));

                        transaction.ConstantSymbol = ParseConstantSymbol(record.ConstantSymbol, constantSymbolsList);

                        transaction.SenderAccount = ParseAccount(string.IsNullOrEmpty(record.Account) ? DefaultUserAccount : record.Account, accountList);
                        transaction.RecipientAccount = ParseAccount(record.RecipientAccount, accountList);

                        file.Transactions.Add(transaction);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Error parsing import data: " + e.ToString());
                        continue;
                    }
                    succesfullyParsedLineCount++;
                }

                if (succesfullyParsedLineCount == 0)
                    return 0;

                user.Files.Add(file);
                model.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine("Import exception:" + e.ToString());
                throw;
            }

            return succesfullyParsedLineCount;
        }

        private Account ParseAccount(string accountToParse, List<Account> accountList)
        {
            Account parsedAccount = Account.CreateAccount(accountToParse);
            if (parsedAccount == null)
                return null;

            Account account = accountList.FirstOrDefault(a => a.Equals(parsedAccount));
            if (account != null)
                return account;

            model.Accounts.Add(parsedAccount);
            accountList.Add(parsedAccount);
            return parsedAccount;
        }

        private ConstantSymbol ParseConstantSymbol(string constantSymbolToParse, List<ConstantSymbol> constantSymbolsList)
        {
            if (!short.TryParse(constantSymbolToParse, out short parsedValue))
                return null;

            ConstantSymbol symbol = constantSymbolsList.FirstOrDefault(c => c.ProvidedID == parsedValue);
            if (symbol != null)
                return symbol;

            symbol = new ConstantSymbol(parsedValue);
            ConstantSymbolOperations.Fill(symbol);
            constantSymbolsList.Add(symbol);
            return symbol;
        }
    }
}
