﻿using System.Collections.Generic;

namespace Core.BusinessLogic.ImportCSVLogic
{
    public interface IModelImporter
    {
        int Import(string filePath, string fileName, long userID);
    }
}