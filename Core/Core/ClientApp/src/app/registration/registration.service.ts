import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
}
)
export class RegistrationService {

  constructor(private HttpClient: HttpClient) { }

  register(username: string, password: string, passwordCheck: string) {
    // better safe than sorry (1. check defined in *.html)
    if (password != passwordCheck) {
      alert('Hesla se musejí shodovat');
      return;
    }

    return this.HttpClient.post<{ username: string, status: number, messasge: string }>('api/Registration', { username, password });
  }
}
