import { Component } from '@angular/core';
import { RegistrationService } from './registration.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
/** registration component*/
export class RegistrationComponent {

  constructor(private Router: Router, private registrationService: RegistrationService, private FormBuilder: FormBuilder) { }

  InsertForm: FormGroup;
  Username: FormControl;
  Password: FormControl;
  PasswordCheck: FormControl;
  ErrorMessage: string = '';
  IsRegistrationValid: boolean = false;

  ngOnInit() {
    this.Username = new FormControl('', [Validators.required]);
    this.Password = new FormControl('', [Validators.required]);
    this.PasswordCheck = new FormControl('', [Validators.required, Validators.pattern(this.Password.value)]);

    this.InsertForm = this.FormBuilder.group({
      "Username": this.Username,
      "Password": this.Password,
      "PasswordCheck": this.PasswordCheck
    });
  }

  registerUser(event): void {
    event.preventDefault();

    const target = event.target;
    const username = target.querySelector('#username').value;
    const password = target.querySelector('#password').value;
    const passwordCheck = target.querySelector('#passwordCheck').value;

    this.registrationService.register(username, password, passwordCheck).subscribe((res) => {
      //console.log('Registration successful: ', res.status.toString());
      this.IsRegistrationValid = false;
      if (res.status = 1) {
        this.IsRegistrationValid = true;
        this.Router.navigate(['login']);
      }
        
    }, error => {
        console.error(error);
        this.IsRegistrationValid = false;
        this.ErrorMessage = "Uživatelské jméno už existuje.";
    });
  }
}
