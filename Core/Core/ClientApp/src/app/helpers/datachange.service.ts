import { Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { IFile } from '../interfaces/IFile';
import { IRequestFiles } from '../interfaces/IRequestFiles';
import { HomeService } from '../home/home.service';

@Injectable()
export class DatachangeService {
  private ImportHistoryTable: MatTableDataSource<IFile>;

  constructor(private HttpClient: HttpClient, private HomeService: HomeService) {
    this.ImportHistoryTable = new MatTableDataSource<IFile>([]);
    this.ReloadImportHistoryTableData();
  }

  public GetImportHistoryTable(): MatTableDataSource<IFile> { return this.ImportHistoryTable }

  public OnDataChange(): void {
    this.ReloadImportHistoryTableData();
    this.HomeService.ReloadData();
  }

  public ReloadImportHistoryTableData(): void {
    this.HttpClient.get<IRequestFiles>('api/Import').subscribe(res => {
      console.log('Get import data: ', res);
      let fileArray = new Array<IFile>();
      for (let i: number = 0; i < res.imports.length; i++) {
        fileArray.push({ position: i + 1, id: res.imports[i].id, name: res.imports[i].name });
      }
      this.ImportHistoryTable.data = fileArray;
    }, error => {
      console.error("Get FilesData error:", error);
    });
  }
}
