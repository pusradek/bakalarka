"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var material_1 = require("@angular/material");
var czechRangeLabel = function (page, pageSize, length) {
    if (length == 0 || pageSize == 0) {
        return "0 z " + length;
    }
    length = Math.max(length, 0);
    var startIndex = page * pageSize;
    // If the start index exceeds the list length, do not try and fix the end index to the end.
    var endIndex = startIndex < length ?
        Math.min(startIndex + pageSize, length) :
        startIndex + pageSize;
    return startIndex + 1 + " - " + endIndex + " z " + length;
};
function getCzechPaginatorIntl() {
    var paginatorIntl = new material_1.MatPaginatorIntl();
    paginatorIntl.itemsPerPageLabel = 'Položek na stránku:';
    paginatorIntl.nextPageLabel = 'Další stránka';
    paginatorIntl.previousPageLabel = 'Předchozí stránka';
    paginatorIntl.getRangeLabel = czechRangeLabel;
    return paginatorIntl;
}
exports.getCzechPaginatorIntl = getCzechPaginatorIntl;
//# sourceMappingURL=czech-paginator-intl.js.map