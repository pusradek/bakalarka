import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from "@angular/common/http";
import { LoginService } from "../login/login.service";
import { Observable, throwError } from "rxjs";
import { catchError } from 'rxjs/operators';
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})

export class JwtInterceptor implements HttpInterceptor {

  constructor(private LoginService: LoginService, private router: Router) { }

  private handleError(err: HttpErrorResponse): Observable<any> {
    let errorMsg;
    if (err.error instanceof Error) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMsg = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMsg = `Backend returned code ${err.status}, body was: ${err.error}`;
    }
    if (err.status === 401) {
      if (this.router.url === '/login' || this.router.url === '')
        return throwError(err);

      console.log("Unauthorized access, logging out...");
      this.LoginService.logout();
      return new Observable();
    }

    console.log(errorMsg);
    return throwError(err);
  }


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let currentuser = this.LoginService.IsLoggedIn;
    let token = localStorage.getItem('jwt');

    if (currentuser && token !== undefined)
      request = request.clone({ setHeaders: { Authorization: `Bearer ${token}` } });

    return next.handle(request).pipe(catchError(x => this.handleError(x)));
  }
}
