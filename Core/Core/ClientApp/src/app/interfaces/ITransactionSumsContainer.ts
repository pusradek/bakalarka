import { ITransaction } from "./ITransaction";

export interface ITransactionSumsContainer {
  expenses: ITransaction[];
  expensesPerSymbolCategory: ITransaction[];
  expensesPerSymbolType: ITransaction[];
  expensesPerSize: ITransaction[];
}
