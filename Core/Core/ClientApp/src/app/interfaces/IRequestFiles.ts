export interface IRequestFiles {
  count: number;
  imports: Array<{ id: number, name: string }>
}
