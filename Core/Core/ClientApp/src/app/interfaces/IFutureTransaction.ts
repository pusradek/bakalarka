export interface IFutureTransaction {
  position: number;
  name: string;
  amount: string;
}
