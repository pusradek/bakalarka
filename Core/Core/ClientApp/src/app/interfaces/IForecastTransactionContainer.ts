import { ITransaction } from "./ITransaction";

export interface IForecastTransactionContainer {
  averagePrediction: ITransaction[][];
  amountPredictionSizeTypes: ITransaction[];
  amountPredictionKinds: ITransaction[];
  amountPredictionList: ITransaction[];
  neuralPrediction: ITransaction[];
  neuralPredictionSizeTypes: ITransaction[];
}
