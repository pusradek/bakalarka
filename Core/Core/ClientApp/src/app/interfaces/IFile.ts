export interface IFile {
  position: number;
  id: number;
  name: string;
}
