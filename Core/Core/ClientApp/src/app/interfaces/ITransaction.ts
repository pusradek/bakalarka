export interface ITransaction {
  amount: number;
  count: number;
  name: string;
  sizeType: string;
  repetitive: boolean;
}
