import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

const loginErrorMessage: string = "Neplatné uživatelské jméno nebo heslo";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
/** login component*/
export class LoginComponent implements OnInit {

  constructor(private Router: Router, private LoginService: LoginService, private FormBuilder: FormBuilder) { }

  InsertForm: FormGroup;
  Username: FormControl;
  Password: FormControl;
  ErrorMessage: string = '';
  IsLoginValid: boolean = false;

  ngOnInit() {
    this.Username = new FormControl('', [Validators.required]);
    this.Password = new FormControl('', [Validators.required]);

    this.InsertForm = this.FormBuilder.group({
      "Username": this.Username,
      "Password": this.Password
    });
  }

  loginUser(event): void {
    const target = event.target;
    const username = target.querySelector('#username').value;
    const password = target.querySelector('#password').value;

    this.LoginService.login(username, password).subscribe(() => {
      if (localStorage.getItem('loginStatus') == '1') {
        this.Router.navigate(['home']);
        this.IsLoginValid = true;
      } else {
        this.IsLoginValid = false;
        this.ErrorMessage = loginErrorMessage;
      }

    }, error => {
      console.error(error);
      this.IsLoginValid = false;
        this.ErrorMessage = loginErrorMessage;
    });
  }
}
