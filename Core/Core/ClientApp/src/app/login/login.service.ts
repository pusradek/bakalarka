import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
}
)
export class LoginService {

  private LoginStatus = new BehaviorSubject<boolean>(this.CheckLoginStatus());
  private UserName = new BehaviorSubject<string>(localStorage.getItem('username'));

  private CheckLoginStatus(): boolean {
    return false;
  }

  get IsLoggedInBehaviorSubject(): BehaviorSubject<boolean> {
    if (this.LoginStatus.value)
      return this.LoginStatus;

    this.LoginByCookie();//user is maybe comeing back -> try to login by cookie
    return this.LoginStatus;
  }

  get IsLoggedIn(): Observable<boolean> {
    if (this.LoginStatus.value)
      this.LoginStatus.asObservable();

    //user is maybe comeing back -> try to login by cookie
    this.LoginByCookie();

    return this.LoginStatus.asObservable();
  }

  get CurrentUsername(): Observable<string> {
    return this.UserName.asObservable();
  }

  constructor(private HttpClient: HttpClient, private Router: Router) { }

  private LoginByCookie(): boolean {
    let loginstatus = localStorage.getItem('loginStatus');
    if (!loginstatus || loginstatus !== '1')
      return false;

    let storedDate = localStorage.getItem('expiration');
    if (!storedDate || /^\s*$/.test(storedDate))//blank / empty / undefined
      return false;

    let parsedStoredDate = new Date(storedDate);
    if (!parsedStoredDate || new Date() > parsedStoredDate) //null or expired date
      return false;

    //JWT token will be checked with first api call
    this.LoginStatus.next(true);
    return true;
  }

  login(username: string, password: string) {
    return this.HttpClient.post<any>('api/Login', { username, password }).pipe(
      map(result => {
        if (result && result.token) {
          this.LoginStatus.next(true);// next = aassigment for observable
          localStorage.setItem('loginStatus', '1');
          localStorage.setItem('jwt', result.token);
          localStorage.setItem('username', result.username);
          localStorage.setItem('expiration', result.expiration);
        }
        return result;
      }));
  }

  logout() {
    this.LoginStatus.next(false);
    localStorage.removeItem('jwt');
    localStorage.removeItem('username');
    localStorage.removeItem('expiration');
    localStorage.setItem('loginStatus', '0');

    this.Router.navigate(['login']);
    console.log('logout successful')
  }

  loginUser(username: string, password: string) {
    return this.HttpClient.post<{ item1: number, item2: string }>('api/Login', { username, password });
  }
}
