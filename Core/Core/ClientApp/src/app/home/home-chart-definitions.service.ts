import { Injectable } from '@angular/core';
import { Chart } from 'angular-highcharts';

@Injectable()
export class HomeChartDefinitionsService {

  constructor() {
    this.MakeCharts();
  }

  //history
  public Expenses: Array<{ name: string, y: number }>; //weekBarChart
  public TrasactionPerSizeTypes: Array<{ name: string, y: number }>;
  public TrasactionPerSymbolCategory: Array<{ name: string, y: number }>;
  public TrasactionPerSymbolType: Array<{ name: string, y: number }>;
  

  //forecast
  public ForecastTypes: Array<{ name: string, y: number }>;
  public ForecastAverageLineData: Array<{ name: string, y: number }>;
  public ForecastAveragePieData: Array<{ name: string, y: number }>;
  public ForecastAmountComparsionKindPieData: Array<{ name: string, y: number }>;
  public ForecastAmountComparsionSizePieData: Array<{ name: string, y: number }>;

  public HistoryWeeksBarChart: Chart;
  public HistoryPerSizePieChart: Chart;
  public HistorySymbolCategoryPieChart: Chart;
  public HistorySymbolTypePieChart: Chart;
  public ForecastPieChart: Chart;
  public ForecastAverageLineChart: Chart;
  public ForecastAveragePieChart: Chart;
  public ForecastAmountComparsionKindPieChart: Chart;
  public ForecastAmountComparsionSizePieChart: Chart;

  public ReloadAll(): void {
    this.ReloadHistoryChartSeries();
    this.ReloadForecastChartSeries();
  }


  public ReloadHistoryChartSeries() {
    this.HistoryWeeksBarChart.removeSeries(0);
    this.HistoryPerSizePieChart.removeSeries(0);
    this.HistorySymbolCategoryPieChart.removeSeries(0);
    this.HistorySymbolTypePieChart.removeSeries(0);

    this.HistoryWeeksBarChart.addSeries({ name: 'Výdaje', data: this.Expenses } as Highcharts.SeriesColumnOptions, true, false);
    this.HistoryPerSizePieChart.addSeries({ name: 'Výdaje', data: this.TrasactionPerSizeTypes } as Highcharts.SeriesColumnOptions, true, false);
    this.HistorySymbolCategoryPieChart.addSeries({ name: 'Výdaje', data: this.TrasactionPerSymbolCategory } as Highcharts.SeriesColumnOptions, true, false);
    this.HistorySymbolTypePieChart.addSeries({ name: 'Výdaje', data: this.TrasactionPerSymbolType } as Highcharts.SeriesColumnOptions, true, false);

    this.HistoryPerSizePieChart.ref.reflow();
    this.HistoryWeeksBarChart.ref.reflow();
    this.HistorySymbolCategoryPieChart.ref.reflow();
    this.HistorySymbolTypePieChart.ref.reflow();
  }

  public ReloadForecastChartSeries() {
    this.ForecastPieChart.removeSeries(0);
    this.ForecastAverageLineChart.removeSeries(0);
    this.ForecastAveragePieChart.removeSeries(0);
    this.ForecastAmountComparsionKindPieChart.removeSeries(0);
    this.ForecastAmountComparsionSizePieChart.removeSeries(0);

    this.ForecastPieChart.addSeries({ name: 'Výdaje', data: this.ForecastTypes } as Highcharts.SeriesColumnOptions, true, false);
    this.ForecastAverageLineChart.addSeries({ name: 'Výdaje', data: this.ForecastAverageLineData } as Highcharts.SeriesColumnOptions, true, false);
    this.ForecastAveragePieChart.addSeries({ name: 'Výdaje', data: this.ForecastAveragePieData } as Highcharts.SeriesColumnOptions, true, false);
    this.ForecastAmountComparsionKindPieChart.addSeries({ name: 'Výdaje', data: this.ForecastAmountComparsionKindPieData } as Highcharts.SeriesColumnOptions, true, false);
    this.ForecastAmountComparsionSizePieChart.addSeries({ name: 'Výdaje', data: this.ForecastAmountComparsionSizePieData } as Highcharts.SeriesColumnOptions, true, false);

    this.ForecastPieChart.ref.reflow();
    this.ForecastAverageLineChart.ref.reflow();
    this.ForecastAveragePieChart.ref.reflow();
    this.ForecastAmountComparsionKindPieChart.ref.reflow();
    this.ForecastAmountComparsionSizePieChart.ref.reflow();
  }

  private MakeCharts() {
    this.HistoryWeeksBarChart = new Chart({
      chart: {
        type: 'column'
      },
      title: {
        text: 'Útrata za poslední čtyři týdny'
      },
      credits: {
        enabled: false
      },
      xAxis: {
        categories: ['1. týden', '2. týden', '3. týden', '4. týden']
      },
      yAxis: {
        labels: { format: '{value} Kč' },
        title: { text: 'Výše útraty (Kč)' },
      },
      tooltip: {
        valueDecimals: 2,
        valueSuffix: ' Kč',
        pointFormat: 'celková velikost: {point.y}<br/>'
          + 'počet transakcí: {point.z}'
      },
      lang: {
        noData: "Žádná data k zobrazení"
      },
      series: [
        {
          name: 'Výdaje',
          data: this.Expenses
        } as Highcharts.SeriesColumnOptions
      ]
    });

    this.HistoryPerSizePieChart = new Chart({
      chart: {
        type: 'pie'
      },
      title: {
        text: 'Celková velikost transakcí'
      },
      credits: {
        enabled: false
      },
      tooltip: {
        valueDecimals: 2,
        valueSuffix: ' Kč',
        pointFormat: 'celková velikost: {point.y}<br/>'
          + 'počet transakcí: {point.z}'
      },
      lang: {
        noData: "Žádná data k zobrazení"
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: [
        {
          name: 'Výdaje',
          data: this.TrasactionPerSizeTypes
        } as Highcharts.SeriesColumnOptions
      ]
    });

    this.HistorySymbolCategoryPieChart = new Chart({
      chart: {
        type: 'pie'
      },
      title: {
        text: 'Třída transakcí'
      },
      credits: {
        enabled: false
      },
      tooltip: {
        valueDecimals: 2,
        valueSuffix: ' Kč',
        pointFormat: 'celková velikost: {point.y}<br/>'
          + 'počet transakcí: {point.z}'
      },
      lang: {
        noData: "Žádná data k zobrazení"
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: [
        {
          name: 'Výdaje',
          data: this.TrasactionPerSymbolCategory
        } as Highcharts.SeriesColumnOptions
      ]
    });

    this.HistorySymbolTypePieChart = new Chart({
      chart: {
        type: 'pie'
      },
      title: {
        text: 'Typ transakcí'
      },
      credits: {
        enabled: false
      },
      tooltip: {
        valueDecimals: 2,
        valueSuffix: ' Kč',
        pointFormat: 'celková velikost: {point.y}<br/>'
          + 'počet transakcí: {point.z}'
      },
      lang: {
        noData: "Žádná data k zobrazení"
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: [
        {
          name: 'Výdaje',
          data: this.TrasactionPerSymbolType
        } as Highcharts.SeriesColumnOptions
      ]
    });

    this.ForecastPieChart = new Chart({
      chart: {
        type: 'pie'
      },
      title: {
        text: 'Předpovídaná celková velikost transakcí'
      },
      credits: {
        enabled: false
      },
      tooltip: {
        valueDecimals: 2,
        valueSuffix: ' Kč'
      },
      lang: {
        noData: "Žádná data k zobrazení"
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: [
        {
          name: 'Line 1',
          data: this.ForecastTypes
        } as Highcharts.SeriesColumnOptions
      ]
    });

    this.ForecastAverageLineChart = new Chart({
      chart: {
        type: 'line'
      },
      title: {
        text: 'Vývoj celkové velikosti transakcí na následující čtyři týdny'
      },
      credits: {
        enabled: false
      },
      xAxis: {
        allowDecimals: false,
        labels: {
          formatter: function () {
            return this.value+1 + ' .den';
          }
        }
      },
      yAxis: {
        labels: { format: '{value} Kč' },
        title: { text: 'Výše útraty (Kč)' },
      },
      tooltip: {
        headerFormat: '',
        valueDecimals: 2,
        valueSuffix: ' Kč',
        pointFormat: 'celková velikost: {point.y}<br/>'
          + 'počet transakcí: {point.z: .2f}'
      },
      lang: {
        noData: "Žádná data k zobrazení"
      },
      series: [
        {
          name: 'Výdaje',
          data: this.ForecastAverageLineData
        } as Highcharts.SeriesColumnOptions
      ]
    });

    this.ForecastAveragePieChart = new Chart({
      chart: {
        type: 'pie'
      },
      title: {
        text: 'Pravděpodobná celková velikost transakcí následující týden'
      },
      credits: {
        enabled: false
      },
      tooltip: {
        valueDecimals: 2,
        valueSuffix: ' Kč',
        pointFormat: 'celková velikost: {point.y}<br/>'
          + 'počet transakcí: {point.z: .2f}'
      },
      lang: {
        noData: "Žádná data k zobrazení"
      },
      series: [
        {
          name: 'Line 1',
          data: this.ForecastAveragePieData
        } as Highcharts.SeriesColumnOptions
      ]
    });

    this.ForecastAmountComparsionKindPieChart = new Chart({
      chart: {
        type: 'pie'
      },
      title: {
        text: 'Druh výdajů na následující týden'
      },
      credits: {
        enabled: false
      },
      tooltip: {
        valueDecimals: 2,
        valueSuffix: ' Kč',
        pointFormat: 'celková velikost: {point.y}<br/>'
          + 'počet transakcí: {point.z}'
      },
      lang: {
        noData: "Žádná data k zobrazení"
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: [
        {
          name: 'Line 1',
          data: this.ForecastAmountComparsionKindPieData
        } as Highcharts.SeriesColumnOptions
      ]
    });

    this.ForecastAmountComparsionSizePieChart = new Chart({
      chart: {
        type: 'pie'
      },
      title: {
        text: 'Celková velikost výdajů na následující týden'
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      credits: {
        enabled: false
      },
      tooltip: {
        valueDecimals: 2,
        valueSuffix: ' Kč',
        pointFormat: 'celková velikost: {point.y}<br/>'
        + 'počet transakcí: {point.z}'
      },
      lang: {
        noData: "Žádná data k zobrazení"
      },
      series: [
        {
          name: 'Line 1',
          data: this.ForecastAmountComparsionSizePieData
        } as Highcharts.SeriesColumnOptions
      ]
    });
  }
}
