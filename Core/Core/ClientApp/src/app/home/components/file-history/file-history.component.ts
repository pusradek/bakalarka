import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { HttpClient } from '@angular/common/http';
import { DatachangeService } from '../../../helpers/datachange.service';
import { IFile } from '../../../interfaces/IFile';

@Component({
  selector: 'app-file-history',
  templateUrl: './file-history.component.html',
  styleUrls: ['./file-history.component.css']
})
/** fileHistory component*/
export class FileHistoryComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  dataSource: MatTableDataSource<IFile>;
  displayedColumns: string[] = ['position', 'name', 'delete'];

  constructor(private HttpClient: HttpClient, private DatachangeService: DatachangeService) {
  }

  ngOnInit() {
    this.DatachangeService.ReloadImportHistoryTableData();
    this.dataSource = this.DatachangeService.GetImportHistoryTable();
    this.dataSource.paginator = this.paginator;
  }

  onDeleteFile(element: IFile): void {
    this.HttpClient.delete('api/Import/' + element.id).subscribe(res => {
      console.log('Deleted file: ', res);
      this.DatachangeService.OnDataChange();
    }, error => {
        console.error("Deleting file error:", error);
    });
  }
}
