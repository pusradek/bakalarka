import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { MatFormFieldModule, MatDialogModule, MatInputModule, MatMenuModule } from '@angular/material'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { JwtInterceptor } from "../../../helpers/jwt.interceptor";
import { AccountPropertiesComponent } from "./account-properties.component";
import { ChangePasswordComponent } from "./change-password/change-password.component";
import { DeleteUserComponent } from "./delete-user/delete-user.component";


@NgModule({
  declarations: [
    AccountPropertiesComponent,
    ChangePasswordComponent,
    DeleteUserComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    BrowserAnimationsModule
  ],
  exports: [
    AccountPropertiesComponent
  ],
  entryComponents: [
    ChangePasswordComponent,
    DeleteUserComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
  ]
})
export class AccountPropertiesModule { }
