import { Component } from '@angular/core';
import { LoginService } from '../../../login/login.service';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DeleteUserComponent } from './delete-user/delete-user.component';

@Component({
    selector: 'app-account-properties',
    templateUrl: './account-properties.component.html',
    styleUrls: ['./account-properties.component.css']
})
/** AccountProperties component*/
export class AccountPropertiesComponent {
/** AccountProperties ctor */
  constructor(private LoginService: LoginService, public dialog: MatDialog) { }

  logout(): void {
    console.log('logout');
    this.LoginService.logout();
  }

  animal: string;
    name: string;

    openDeleteUserDialog(): void {
        const dialogRef = this.dialog.open(DeleteUserComponent, {
            width: '250px',
            data: { name: this.name, animal: this.animal }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this.animal = result;
        });
    }

    openChangePasswordDialog(): void {
    const dialogRef = this.dialog.open(ChangePasswordComponent, {
      width: '250px',
      data: { name: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }
}
