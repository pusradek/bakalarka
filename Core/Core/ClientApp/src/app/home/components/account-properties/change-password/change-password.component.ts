import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { IDialogData } from '../../../../interfaces/IDialogData';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
/** ChangePassword component*/
export class ChangePasswordComponent implements OnInit {
  Password: FormControl;
  NewPassword: FormControl;
  PasswordCheck: FormControl;
  InsertForm: FormGroup;
  IsChangeValid: boolean = false;
  ErrorMessage: string = "";

  /** ChangePassword ctor */
  constructor(
    public dialogRef: MatDialogRef<ChangePasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IDialogData,
    private FormBuilder: FormBuilder,
    private HttpClient: HttpClient
  ) { }

  ngOnInit() {
    this.Password = new FormControl('', [Validators.required]);
    this.NewPassword = new FormControl('', [Validators.required]);
    this.PasswordCheck = new FormControl('', [Validators.required, Validators.pattern(this.Password.value)]);

    this.InsertForm = this.FormBuilder.group({
      "Password": this.Password,
      "NewPassword": this.NewPassword,
      "PasswordCheck": this.PasswordCheck
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onChangeClick(event): void {
    this.changePassword(event);
  }

  changePassword(event): void {
    event.preventDefault();

    const target = event.target;
    const oldPassword = target.querySelector('#password').value;
    const newPassword = target.querySelector('#newPassword').value;

    this.HttpClient.put<{ username: string, status: number, messasge: string }>('api/Registration', { oldPassword, newPassword }).subscribe((res) => {
      //console.log('Password update successful: ', res.status.toString());
      this.IsChangeValid = false;
      if (res.status = 1) {
        this.IsChangeValid = true;
        this.dialogRef.close();
      }
    }, error => {
      console.error("changig password error:", error);
      this.IsChangeValid = false;
      this.ErrorMessage = "Nesprávné heslo";
    });
  }
}
