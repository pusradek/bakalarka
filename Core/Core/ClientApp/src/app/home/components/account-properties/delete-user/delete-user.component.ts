import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { IDialogData } from '../../../../interfaces/IDialogData';
import { LoginService } from '../../../../login/login.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
/** ChangePassword component*/
export class DeleteUserComponent implements OnInit {
  Password: FormControl;
  InsertForm: FormGroup;
  IsChangeValid: boolean = false;
  ErrorMessage: string = "";

  /** ChangePassword ctor */
  constructor(
    public dialogRef: MatDialogRef<DeleteUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IDialogData,
    private FormBuilder: FormBuilder,
    private HttpClient: HttpClient,
    private LoginService: LoginService
  ) { }

  ngOnInit() {
    this.Password = new FormControl('', [Validators.required]);
    this.InsertForm = this.FormBuilder.group({ "Password": this.Password });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onChangeClick(event): void {
    this.deleteUser(event);
  }

  deleteUser(event): void {
    event.preventDefault();

    const target = event.target;
    const password: string = target.querySelector('#password').value;

    this.HttpClient.request<{ status: number, messasge: string }>('delete', 'api/Registration', { body: { password } }).subscribe((res: any) => {
      this.IsChangeValid = false;
      if (res.status = 1) {
        this.IsChangeValid = true;
        this.dialogRef.close();
        this.LoginService.logout();
      }
    }, error => {
      console.error("Deletion of user error:", error);
      this.IsChangeValid = false;
      this.ErrorMessage = "Nesprávné heslo";
    });
  }
}
