import { Component } from '@angular/core';
import { ImportService } from './import.service';
import { DatachangeService } from '../../../helpers/datachange.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css']
})
/** Import component*/
export class ImportComponent {
  fileToUpload: File = null;

  public InfoMessage: string;
  public WarnMessage: string;
  public ErrorMessage: string;

  /** Import ctor */
  constructor(private ImportService: ImportService, private DatachangeService: DatachangeService) { }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  uploadFileToActivity() {
    this.SetWarnMessage("Soubor je nahráván...");
    this.ImportService.postFile(this.fileToUpload).subscribe(data => {
      this.SetInfoMessage("Soubor úspěšně nahrán");
      this.fileToUpload = null;
      this.DatachangeService.OnDataChange();
    }, error => {
      this.SetErrorMessage("Soubor se nepodařilo nahrát");

      if (!(error instanceof HttpErrorResponse && (<HttpErrorResponse>error).status === 400))
        console.log(error);
    });
  }

  private SetInfoMessage(msg: string): void {
    this.WarnMessage = this.ErrorMessage = "";
    this.InfoMessage = msg;
  }

  private SetWarnMessage(msg: string): void {
    this.InfoMessage = this.ErrorMessage = "";
    this.WarnMessage = msg;
  }

  private SetErrorMessage(msg: string): void {
    this.WarnMessage = this.InfoMessage = "";
    this.ErrorMessage = msg;
  }

  openInput() {
    document.getElementById("fileInput").click();
  }
}
