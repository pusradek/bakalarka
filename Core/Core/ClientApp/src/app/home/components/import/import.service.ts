import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
}
)
export class ImportService {

  constructor(private HttpClient: HttpClient) { }

  postFile(fileToUpload: File): Observable<Object> {
    const formData: FormData = new FormData();
    formData.append('fileKey', fileToUpload, fileToUpload.name);
    return this.HttpClient.post('api/Import', formData);
  }
}
