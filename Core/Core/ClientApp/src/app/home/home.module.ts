import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ChartModule, HIGHCHARTS_MODULES } from "angular-highcharts";
import NoDataToDisplay from 'highcharts/modules/no-data-to-display';
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JwtInterceptor } from "../helpers/jwt.interceptor";

import { HomeComponent } from "./home.component";
import { ImportComponent } from "./components/import/import.component";
import { ImportService } from "./components/import/import.service";
import { AccountPropertiesModule } from "./components/account-properties/account-properties.module";
import { FileHistoryComponent } from "./components/file-history/file-history.component";
import { MatTableModule, MatPaginatorModule, MatPaginatorIntl, MatButtonModule } from "@angular/material";
import { HomeService } from "./home.service";
import { HomeChartDefinitionsService } from "./home-chart-definitions.service";
import { getCzechPaginatorIntl } from "../helpers/czech-paginator-intl";
import { DatachangeService } from "../helpers/datachange.service";


@NgModule({
  declarations: [
    HomeComponent,
    ImportComponent,
    FileHistoryComponent
  ],
  imports: [
    CommonModule,
    ChartModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    ReactiveFormsModule,
    AccountPropertiesModule
  ],
  providers: [
    ImportService,
    HomeService,
    HomeChartDefinitionsService,
    DatachangeService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: MatPaginatorIntl, useValue: getCzechPaginatorIntl() },
    { provide: HIGHCHARTS_MODULES, useFactory: () => [NoDataToDisplay] }
  ]
})
export class HomeModule { }
