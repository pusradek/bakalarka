import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginService } from '../login/login.service';
import { Chart } from 'angular-highcharts';
import { HomeService } from './home.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { IFutureTransaction } from '../interfaces/IFutureTransaction';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
/** home component*/
export class HomeComponent implements OnInit {
  @ViewChild('NeuralNetworkPaginator', { static: true })
  NeuralNetworkTablePaginator: MatPaginator;
  @ViewChild('AmountPredictionPaginator', { static: true })
  AmountPredictionTablePaginator: MatPaginator;
  NeuralNetworkTableDataSource: MatTableDataSource<IFutureTransaction>;
  AmountPredictionTableDataSource: MatTableDataSource<IFutureTransaction>;
  NeuralNetworkTableDisplayedColumns: string[] = ['position', 'name', 'amount'];
  AmountPredictionTableDisplayedColumns: string[] = ['position', 'name', 'amount'];


  LoginStatus$: Observable<boolean>;
  Username$: Observable<string>;

  public GetHistoryPerSizePie: Chart;
  public HistoryWeeksBarChart: Chart;
  public HistorySymbolCategoryPieChart: Chart;
  public HistorySymbolTypePieChart: Chart;

  public ForecastPieChart: Chart;
  public ForecastAverageLineChart: Chart;
  public ForecastAveragePieChart: Chart;
  public ForecastAmountComparsionKindPieChart: Chart;
  public ForecastAmountComparsionSizePieChart: Chart;

  /** home ctor */
  constructor(private LoginService: LoginService, private HomeService: HomeService) {
  }

  ngOnInit() {
    this.NeuralNetworkTableDataSource = this.HomeService.NeuralNetworkTableDataSource;
    this.AmountPredictionTableDataSource = this.HomeService.AmountPredictionTableDataSource;

    this.NeuralNetworkTableDataSource.paginator = this.NeuralNetworkTablePaginator;
    this.AmountPredictionTableDataSource.paginator = this.AmountPredictionTablePaginator;

    this.LoginStatus$ = this.LoginService.IsLoggedIn;
    this.Username$ = this.LoginService.CurrentUsername;

    this.InitCharts();
  }

  private InitCharts(): void {
    this.GetHistoryPerSizePie = this.HomeService.GetHistoryPerSizePie();
    this.HistoryWeeksBarChart = this.HomeService.GetHistoryWeeksBar();
    this.HistorySymbolCategoryPieChart = this.HomeService.GetHistorySymbolCategoryPieChart();
    this.HistorySymbolTypePieChart = this.HomeService.GetHistorySymbolTypePieChart();

    this.ForecastPieChart = this.HomeService.GetForecastPie();
    this.ForecastAverageLineChart = this.HomeService.GetForecastAverageLine();
    this.ForecastAveragePieChart = this.HomeService.GetForecastAveragePie();
    this.ForecastAmountComparsionKindPieChart = this.HomeService.GetForecastAmountComparsionKindPie();
    this.ForecastAmountComparsionSizePieChart = this.HomeService.GetForecastAmountComparsionSizePie();

    this.HomeService.ReloadData(); //after re-loggin fire up graphs
  }

  onLogout() {
    this.LoginService.logout();
  }
}
