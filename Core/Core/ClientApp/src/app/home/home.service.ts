import { Injectable } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { HttpClient } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material';
import { HomeChartDefinitionsService } from './home-chart-definitions.service';
import { IFutureTransaction } from '../interfaces/IFutureTransaction';
import { ITransactionSumsContainer } from '../interfaces/ITransactionSumsContainer';
import { ITransaction } from '../interfaces/ITransaction';
import { IForecastTransactionContainer } from '../interfaces/IForecastTransactionContainer';

@Injectable()
export class HomeService {
  NeuralNetworkTableDataSource: MatTableDataSource<IFutureTransaction>;
  AmountPredictionTableDataSource: MatTableDataSource<IFutureTransaction>;

  constructor(private HttpClient: HttpClient, private HomeChartDefinitionsService: HomeChartDefinitionsService) {
    this.NeuralNetworkTableDataSource = new MatTableDataSource<IFutureTransaction>([]);
    this.AmountPredictionTableDataSource = new MatTableDataSource<IFutureTransaction>([]);
    this.ReloadData();
  }

  public ReloadData(): void {
    this.LoadHistoryData();
    this.LoadForecastData();
  }

  private LoadHistoryData(): void {
    this.HttpClient.get<ITransactionSumsContainer>('api/History').subscribe(res => {
      if (res === null) {
        this.HomeChartDefinitionsService.Expenses = new Array<{ name: string, y: number }>();
        this.HomeChartDefinitionsService.TrasactionPerSizeTypes = new Array<{ name: string, y: number }>();
        this.HomeChartDefinitionsService.TrasactionPerSymbolCategory = new Array<{ name: string, y: number }>();
        this.HomeChartDefinitionsService.TrasactionPerSymbolType = new Array<{ name: string, y: number }>();
      }
      else {
        this.HomeChartDefinitionsService.Expenses = this.MakeLineChartData(res.expenses);
        this.HomeChartDefinitionsService.TrasactionPerSizeTypes = this.MakePieChartData(res.expensesPerSize);
        this.HomeChartDefinitionsService.TrasactionPerSymbolCategory = this.MakePieChartData(res.expensesPerSymbolCategory);
        this.HomeChartDefinitionsService.TrasactionPerSymbolType = this.MakePieChartData(res.expensesPerSymbolType);
      }

      this.HomeChartDefinitionsService.ReloadHistoryChartSeries();
    }, error => {
      console.error("LoadHistoryData error:", error);
    });
  }

  private LoadForecastData(): void {
    this.HttpClient.get<IForecastTransactionContainer>('api/Forecast').subscribe(res => {

      if (res === null) {
        this.NeuralNetworkTableDataSource.data = new Array<IFutureTransaction>();
        this.AmountPredictionTableDataSource.data = new Array<IFutureTransaction>();

        this.HomeChartDefinitionsService.ForecastTypes = new Array<{ name: string, y: number }>();
        this.HomeChartDefinitionsService.ForecastAverageLineData = new Array<{ name: string, y: number }>();
        this.HomeChartDefinitionsService.ForecastAveragePieData = new Array<{ name: string, y: number }>();
        this.HomeChartDefinitionsService.ForecastAmountComparsionKindPieData = new Array<{ name: string, y: number }>();
        this.HomeChartDefinitionsService.ForecastAmountComparsionSizePieData = new Array<{ name: string, y: number }>();
      }
      else {
        this.HomeChartDefinitionsService.ForecastTypes = this.MakePieChartData(res.neuralPredictionSizeTypes);
        this.NeuralNetworkTableDataSource.data = this.MakeMatTableData(res.neuralPrediction);

        //make 1D array for line chart (4x Mon - Sun)
        let continuousData: ITransaction[] = new Array();
        for (let i: number = 0; i < res.averagePrediction.length; i++)
          continuousData = continuousData.concat(res.averagePrediction[i]);
        this.HomeChartDefinitionsService.ForecastAverageLineData = this.MakeLineChartData(continuousData);

        this.AmountPredictionTableDataSource.data = this.MakeMatTableData(res.amountPredictionList);
        this.HomeChartDefinitionsService.ForecastAveragePieData = this.MakeAverageFromWeekDays(res.averagePrediction);
        this.HomeChartDefinitionsService.ForecastAmountComparsionKindPieData = this.MakePieChartData(res.amountPredictionKinds);
        this.HomeChartDefinitionsService.ForecastAmountComparsionSizePieData = this.MakePieChartData(res.amountPredictionSizeTypes);
      }

      this.HomeChartDefinitionsService.ReloadForecastChartSeries();
    }, error => {
      console.error("Get Forecast error:", error);
    });
  }

  private MakeMatTableData(input: ITransaction[]): IFutureTransaction[] {
    let tableData = new Array<IFutureTransaction>();
    for (let i: number = 0; i < input.length; i++) {
      let transaction: ITransaction = input[i];
      if (input[i].amount != 0)
        tableData.push({ position: i + 1, name: transaction.name, amount: Math.abs(transaction.amount).toString() + ' Kč' });
    }
    return tableData;
  }

  private MakeLineChartData(input: ITransaction[]): Array<{ name: string, y: number, z: number }> {
    let lineData = new Array<{ name: string, y: number, z: number }>();

    for (let i: number = 0; i < input.length; i++)
      lineData.push({ name: input[i].name, y: Math.abs(input[i].amount), z: input[i].count });

    return lineData;
  }

  private MakePieChartData(input: ITransaction[]): Array<{ name: string, y: number, z: number }> {
    let pieData = new Array<{ name: string, y: number, z: number }>();

    for (let i: number = 0; i < input.length; i++) {
      if (input[i].amount != 0)
        pieData.push({ name: input[i].name, y: Math.abs(input[i].amount), z: input[i].count });
    }

    return pieData;
  }

  private MakeAverageFromWeekDays(input: ITransaction[][]): Array<{ name: string, y: number, z: number }> {
    let weekDaysData: Array<{ amount: number, count: number, name: string }> = new Array(
      { amount: 0, count: 0, name: "pondělí" },
      { amount: 0, count: 0, name: "úterý" },
      { amount: 0, count: 0, name: "středa" },
      { amount: 0, count: 0, name: "čtvrtek" },
      { amount: 0, count: 0, name: "pátek" },
      { amount: 0, count: 0, name: "sobota" },
      { amount: 0, count: 0, name: "neděle" },
    );

    input.forEach(x => {
      for (let i: number = 0; i < 7; i++) {
        weekDaysData[i].amount += x[i].amount;
        weekDaysData[i].count += x[i].count;
      }
    });

    weekDaysData.forEach(x => { x.amount /= 4.0; x.count /= 4.0 })

    return this.MakePieChartData(weekDaysData as ITransaction[]);
  }

  public GetHistoryWeeksBar(): Chart { return this.HomeChartDefinitionsService.HistoryWeeksBarChart; }
  public GetHistoryPerSizePie(): Chart { return this.HomeChartDefinitionsService.HistoryPerSizePieChart; }
  public GetHistorySymbolCategoryPieChart(): Chart { return this.HomeChartDefinitionsService.HistorySymbolCategoryPieChart; }
  public GetHistorySymbolTypePieChart(): Chart { return this.HomeChartDefinitionsService.HistorySymbolTypePieChart; }
  public GetForecastPie(): Chart { return this.HomeChartDefinitionsService.ForecastPieChart; }
  public GetForecastAverageLine(): Chart { return this.HomeChartDefinitionsService.ForecastAverageLineChart; }
  public GetForecastAveragePie(): Chart { return this.HomeChartDefinitionsService.ForecastAveragePieChart; }
  public GetForecastAmountComparsionKindPie(): Chart { return this.HomeChartDefinitionsService.ForecastAmountComparsionKindPieChart; }
  public GetForecastAmountComparsionSizePie(): Chart { return this.HomeChartDefinitionsService.ForecastAmountComparsionSizePieChart; }
}
