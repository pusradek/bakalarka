﻿using Core.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Network
    {
        [Key]
        public long ID { get; set; }
        public ICollection<Layer> Layer { get; set; }
        public ICollection<User> Users { get; set; }

        public Network()
        {
        }

        public Network(List<List<List<double>>> list)
        {
            Layer = new List<Layer>(list.Count);
            foreach (var l in list)
                Layer.Add(new Layer(l));
        }

        public List<List<List<double>>> ToImportList()
        {
            List<List<List<double>>> list = new List<List<List<double>>>(Layer.Count);
            foreach (Layer l in Layer)
                list.Add(l.ToList());

            return list;
        }
    }

    public class Layer
    {
        [Key]
        public long ID { get; set; }
        public ICollection<Neuron> Neuron { get; set; }

        public Layer()
        {
        }

        public Layer(List<List<double>> list)
        {
            Neuron = new List<Neuron>(list.Count);
            foreach (var l in list)
                Neuron.Add(new Neuron(l));
        }

        public List<List<double>> ToList()
        {
            List<List<double>> list = new List<List<double>>(Neuron.Count);
            foreach (Neuron n in Neuron)
                list.Add(n.ToList());

            return list;
        }
    }

    public class Neuron
    {
        [Key]
        public long ID { get; set; }
        public ICollection<Weight> Weight { get; set; }

        public Neuron()
        {
        }

        public Neuron(List<double> list)
        {
            Weight = new List<Weight>(list.Count);
            foreach (var value in list)
                Weight.Add(new Weight() { Value = value });
        }

        public List<double> ToList() => Weight.Select(w => w.Value).ToList();
    }

    public class Weight
    {
        [Key]
        public long ID { get; set; }
        public double Value { get; set; }
    }
}
