﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class VersionInfo
    {
        public long ID { get; set; }
        public DateTime LastModelChange { get; set; }
    }
}
