﻿using Core.Data;
using Core.Models.Codebooks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    // https://cs.wikipedia.org/wiki/Konstantn%C3%AD_symbol_(pen%C4%9B%C5%BEn%C3%AD_p%C5%99evod)
    public class ConstantSymbol
    {
        public long ID { get; set; }

        public short ProvidedID { get; set; }
        public bool Valid { get; set; }
        public bool? DomesticPayment { get; set; }

        [ForeignKey("Kind")]
        public long? KindID { get; set; }
        [ForeignKey("LastNumber")]
        public long? LastNumberID { get; set; }
        [ForeignKey("MinistryPredefined")]
        public long? MinistryPredefinedID { get; set; }
        [ForeignKey("ReservedSymbol")]
        public long? ReservedSymbolID { get; set; }
        [ForeignKey("ForeignPayment")]
        public long? ForeignPaymentID { get; set; }

        public Kind Kind { get; set; }
        public LastNumber LastNumber { get; set; }
        public MinistryPredefined MinistryPredefined { get; set; }
        public ReservedSymbol ReservedSymbol { get; set; }
        public ForeignPayment ForeignPayment { get; set; }

        private void ForeignInitialization(Model model)
        {
            ForeignPayment = model.ForeignPayments.FirstOrDefault(x => x.Code == ProvidedID % 1000);
            Valid = (ForeignPayment != null);
        }

        /// <summary>
        /// contains precisely four or less ciphers (0 padding from left) and is defined wheter it is domestic payment
        /// </summary>
        public void ValidateThis()
        {
            Valid = ProvidedID < 9999;
        }

        public ConstantSymbol()
        {
        }

        public ConstantSymbol(short value)
        {
            this.ProvidedID = value;
        }
    }
}
