﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models.Codebooks
{
    public class AccountType
    {
        public long ID { get; set; }

        public string Shortname { get; set; }
        public string Fullname { get; set; }

        public ICollection<Account> Accounts { get; set; }
    }
}
