﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models.Codebooks
{
    public abstract class AbstractConstantSymbolVariation
    {
        public long ID { get; set; }

        public short Code { get; set; }
        public string Value { get; set; }

        public ICollection<ConstantSymbol> ConstantSymbols { get; set; }
    }

    // Třídy konstantních symbolů
    public class Kind : AbstractConstantSymbolVariation { }

    // Význam poslední číslice
    public class LastNumber : AbstractConstantSymbolVariation { }

    // Třídy spravované ministerstvem financí (třída 1 + 4)
    public class MinistryPredefined : AbstractConstantSymbolVariation { }

    // Bankovní konstantní symboly
    public class ReservedSymbol : AbstractConstantSymbolVariation { }

    // Zahraniční platby
    public class ForeignPayment : AbstractConstantSymbolVariation { }
}
