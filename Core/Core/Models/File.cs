﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class File
    {
        public long ID { get; set; }
        public User User { get; set; }
        public string Name { get; set; }
        public ICollection<Transaction> Transactions { get; set; }


        //N:1 User
        [ForeignKey("User")]
        public long? UserID { get; set; }
        
    }
}
