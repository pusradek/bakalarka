﻿using Core.BusinessLogic.ImportCSVLogic;
using Core.Models.Codebooks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Transaction
    {
        #region DatabaseKeys
        public long ID { get; set; }
        public string ProvidedID { get; set; }

        public DateTime Date { get; set; }
        public decimal Amount { get; set; }

        public string Note { get; set; }
        public long? VariableSymbol { get; set; }
        public long? SpecificSymbol { get; set; }
        public decimal? SenderAccountBallance { get; set; }

        [ForeignKey("ConstantSymbol")]
        public long? ConstantSymbolID { get; set; }
        [ForeignKey("SenderAccount")]
        public long? SenderAccountID { get; set; }
        [ForeignKey("RecipientAccount")]
        public long? RecipientAccountID { get; set; }

        [ForeignKey("TransactionType")]
        public long? TransactionTypeID { get; set; }

        [ForeignKey("File")]
        public long? FileID { get; set; }

        public ConstantSymbol ConstantSymbol { get; set; }
        public Account SenderAccount { get; set; }
        public Account RecipientAccount { get; set; }
        public TransactionType TransactionType { get; set; }
        public File File { get; set; }

        #endregion


        public static bool IsEqual(Transaction a, Transaction b)
        {
            if ((double)Math.Abs(a.Amount - b.Amount) > 0.0001)
                return false;

            if (a.RecipientAccount != b.RecipientAccount)
                return false;

            return true;
        }

        public Transaction()
        {
        }

        public Transaction(DocumentLayout document) : base()
        {
            this.ProvidedID = document.ID;
            this.Date = DateTime.Parse(document.Date);
            this.Amount = decimal.Parse(document.Amount, NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, new CultureInfo("cs-CZ"));
            this.Note = document.Note;

            if (long.TryParse(document.VariableSymbol, out long parseValue))
                this.VariableSymbol = parseValue;

            if (long.TryParse(document.SpecificSymbol, out parseValue))
                this.SpecificSymbol = parseValue;

            if (long.TryParse(document.SenderAccountBallance, out parseValue))
                this.SenderAccountBallance = parseValue;
        }
    }
}
