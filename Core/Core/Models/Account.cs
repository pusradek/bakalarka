﻿using Core.Models.Codebooks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Account
    {
        public long ID { get; set; }

        public int? AccountPrefix { get; set; }
        public long AccountNumber { get; set; }
        public int BankCode { get; set; }
        public string AccountName { get; set; }
        public decimal? Ballance { get; set; }

        //N:1 Type (SÚ, KK, DK)
        [ForeignKey("AccountType")]
        public long? AccountTypeID { get; set; }

        public AccountType AccountType { get; set; }
        public ICollection<Transaction> SenderTransactions { get; set; }
        public ICollection<Transaction> RecipientTransactions { get; set; }

        //1:N Transaction

        public static Account CreateAccount(string accountNumber)
        {
            if (string.IsNullOrWhiteSpace(accountNumber))
                return null;

            string[] splitedString = accountNumber.Split(new char[] { '-', '/' });

            if (splitedString.Length < 2) //does not have account number or bank code
                return null;

            if (!int.TryParse(splitedString.Last(), out int parsedBankCode))
                return null;

            if (!long.TryParse(splitedString[splitedString.Length - 2], out long parsedAccountNumber))
                return null;

            Account account = new Account() { AccountNumber = parsedAccountNumber, BankCode = parsedBankCode };

            if (splitedString.Length < 3) //account doesn't have prefix
                return account;

            if (!int.TryParse(splitedString[0], out int parsedPrefix))
                return null;

            account.AccountPrefix = parsedPrefix;
            return account;
        }

        public bool Equals(Account another) => BankCode == another.BankCode &&
                AccountNumber == another.AccountNumber &&
                Equals(AccountPrefix, another.AccountPrefix);
    }
}
