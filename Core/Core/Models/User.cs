﻿using Core.BusinessLogic.PasswordLogic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class User
    {
        public User()
        {
        }

        public User(string username, string password)
        {
            Username = username;
            Password = PasswordHasher.Hash(password);
        }

        public long ID { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }
        
        public ICollection<File> Files { get; set; }

        //N:1 Network
        [ForeignKey("Network")]
        public long? NetworkID { get; set; }

        public bool IsEqual(string username, string password)
        {
            return username == Username &&
                PasswordHasher.Check(Password, password);

        }
    }
}
