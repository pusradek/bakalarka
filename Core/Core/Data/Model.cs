﻿using Core.Models;
using Core.Models.Codebooks;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Data
{
    public class Model : DbContext
    {
        public Model(DbContextOptions<Model> options) : base(options)
        { }

        //Basic tables
        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<ConstantSymbol> ConstantSymbols { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Network> Networks { get; set; }
        public DbSet<VersionInfo> VersionInfos { get; set; }

        //Codebooks definying type of constant smybol
        public DbSet<AccountType> AccountTypes { get; set; }
        public DbSet<TransactionType> TransactionTypes { get; set; }
        public DbSet<Kind> Kinds { get; set; }
        public DbSet<LastNumber> LastNumbers { get; set; }
        public DbSet<MinistryPredefined> MinistryPredefined { get; set; }
        public DbSet<ReservedSymbol> ReservedSymbols { get; set; }
        public DbSet<ForeignPayment> ForeignPayments { get; set; }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            SetRelations(modelBuilder);
            SetTableNames(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }

        private void SetRelations(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .HasMany(a => a.RecipientTransactions)
                .WithOne(t => t.RecipientAccount)
                .HasForeignKey(t => t.RecipientAccountID)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<Account>()
                .HasMany(a => a.SenderTransactions)
                .WithOne(t => t.SenderAccount)
                .HasForeignKey(t => t.SenderAccountID)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<Transaction>()
                .HasOne(t => t.File)
                .WithMany(f => f.Transactions)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany(u => u.Files)
                .WithOne(f => f.User)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>().HasIndex(u => u.Username).IsUnique();
        }

        private void SetTableNames(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>().ToTable("Account");
            modelBuilder.Entity<ConstantSymbol>().ToTable("ConstantSymbol");
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<File>().ToTable("Files");
            modelBuilder.Entity<AccountType>().ToTable("AccountType");
            modelBuilder.Entity<TransactionType>().ToTable("TransactionType");
            modelBuilder.Entity<Kind>().ToTable("Kind");
            modelBuilder.Entity<LastNumber>().ToTable("LastNumber");
            modelBuilder.Entity<MinistryPredefined>().ToTable("MinistryPredefined");
            modelBuilder.Entity<ReservedSymbol>().ToTable("ReservedSymbol");
            modelBuilder.Entity<ForeignPayment>().ToTable("ForeignPayment");
            modelBuilder.Entity<Network>().ToTable("Network");
            modelBuilder.Entity<VersionInfo>().ToTable("VersionInfo");
        }
    }
}
