﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Data
{
    public class DbInitializer
    {
        public static void Initialize(Model model, bool rebuildDB=false)
        {
            NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
            try
            {
                if (rebuildDB)
                    model.Database.EnsureDeleted();

                model.Database.EnsureCreated();
            }
            catch (Exception ex)
            {
                logger.Fatal(ex, "Could not read/write Database: " + ex.Message);
                throw;
            }

            // find out if there are any records already
            if (model.VersionInfos.Any())
                return;     // DB has been already created

            model.VersionInfos.Add(new VersionInfo() { LastModelChange = DateTime.Now });
            model.SaveChanges();
        }
    }
}
