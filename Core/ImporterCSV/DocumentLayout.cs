﻿using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImporterCSV
{
    class DocumentLayout
    {
        [Name("2")]
        public string AccountType { get; set; }
        [Name("číslo účtu")]
        public string Account { get; set; }
        [Name("datum zaúčtování")]
        public string Date { get; set; }
        [Name("částka")]
        public decimal Amount { get; set; }
        [Name("měna")]
        public string Currency { get; set; }
        [Name("zůstatek")]
        public string SenderAccountBallance { get; set; }
        [Name("číslo účtu protiúčtu")]
        public string RecipientAccount { get; set; }
        [Name("kód banky protiúčtu")]
        public string RecipientBankCode { get; set; }
        [Name("název účtu protiúčtu")]
        public string RecipientAccountName { get; set; }
        [Name("konstantní symbol")]
        public string ConstantSymbol { get; set; }
        [Name("variabilní symbol")]
        public string VariableSymbol { get; set; }
        [Name("specifický symbol")]
        public string SpecificSymbol { get; set; }
        [Name("označení operace")]
        public string TransactionType { get; set; }
        [Name("ID transakce")]
        public long ID { get; set; }
        [Name("poznámka")]
        public string Note { get; set; }

        public string Dump(string delimiter = "\n")
        {
            string str;

            str = AccountType + delimiter;
            str += Account + delimiter;
            str += Date.ToString() + delimiter;
            str += Amount + delimiter;
            str += Currency + delimiter;
            str += SenderAccountBallance + delimiter;
            str += RecipientAccount + delimiter;
            str += RecipientBankCode + delimiter;
            str += RecipientAccountName + delimiter;
            str += ConstantSymbol + delimiter;
            str += VariableSymbol + delimiter;
            str += SpecificSymbol + delimiter;
            str += TransactionType + delimiter;
            str += ID + delimiter;
            str += Note;

            return str;
        }
    }
}
