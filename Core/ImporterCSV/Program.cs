﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ImporterCSV
{
    class Program
    {
        static void Main(string[] args)
        {
            IEnumerable<DocumentLayout> records;

            using (var reader = new StreamReader("C:\\Users\\Krypton0\\Desktop\\History_01_original.csv"))
            using (var csv = new CsvReader(reader))
            {
                records = csv.GetRecords<DocumentLayout>();


                string text = string.Empty;
                int i = 0;

                foreach (var doc in records)
                {
                    text += doc.Dump(", ") + '\n';
                    if (i++ > 10)
                        break;
                }

                Console.WriteLine(text);
            }
        }
    }
}
