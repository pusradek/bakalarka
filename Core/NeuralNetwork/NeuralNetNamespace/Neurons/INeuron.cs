﻿using MathNet.Numerics.LinearAlgebra.Double;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork.NeuralNetNamespace.Neurons
{
    interface INeuron
    {
        double Evaluate(DenseVector inputVector);

        DenseVector Propagate(double error);
        void AdjustWeights();
        double Output { get; }

        List<double> Export();
        void Import(List<double> list);
    }
}
