﻿using MathNet.Numerics.LinearAlgebra.Double;
using NeuralNetwork.NeuralNetNamespace.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace NeuralNetwork.NeuralNetNamespace.Neurons
{
    class Neuron : INeuron
    {
        private DenseVector Input;

        /// <summary>
        /// output of the neuron (after activation function)
        /// </summary>
        public double Output { get; private set; }
        private int PropagationIterator;

        public DenseVector Weights { get; private set; }
        

        private DenseVector Corrections;
        private const double BIAS = 0.2;

        public Neuron(int weightCount)
        {
            if (weightCount <= 0)
                throw new ArgumentException("weightCount must be greater than 0");

            Weights = new DenseVector(weightCount);
            Corrections = new DenseVector(weightCount);


            // fill neuron with starting weights (random non-zero)
            Random rng = new Random();
            for (int i = 0; i < weightCount; i++)
            {
                double weight = rng.NextDouble()-0.5;
                Weights[i] = weight;
            }
        }

        public Neuron(int weightCount, int outputs)
        {
            if (weightCount <= 0)
                throw new ArgumentException("weightCount must be greater than 0");

            Corrections = new DenseVector(weightCount);

            Weights = (DenseVector) DenseVector.Build.Random(weightCount, new MathNet.Numerics.Distributions.Normal(0.1, 0.9));
            for (int i = 0; i < weightCount; i++)
                if (i % 2 == 0)
                    Weights[i] *= -1;
        }

        public double Evaluate(DenseVector inputVector)
        {
            Input = inputVector;
            Output = (Input * Weights.ToColumnMatrix())[0];
            Output = NetworkLogic.ActivationFunction(Output + BIAS);
            return Output;
        }

        public void AdjustWeights()
        {
            Correct();
            Corrections.Clear();
            PropagationIterator = 0;
        }

        private void Correct()
        {
            if (PropagationIterator == 0)
                return;

            Corrections /= PropagationIterator;

            for (int i = 0; i < Corrections.Count; i++)
            {
                if (double.IsNaN(Corrections[i]))
                    throw new ArgumentOutOfRangeException("parametr correction overflow");

                Weights[i] += Corrections[i];
            }
                
        }

        /// <summary>
        /// propagate error to neuron
        /// </summary>
        /// <param name="error">error of the output</param>
        /// <returns>error for weights in neuron</returns>
        public DenseVector Propagate(double gammaFromLayer)
        {
            double localGamma = gammaFromLayer * (1 - Output * Output);
            Corrections += localGamma * Input;

            PropagationIterator++;
            return Weights * localGamma;
        }

        public override string ToString()
        {
            string str = "0";
            if (Weights != null)
            {
                str = Weights.ToRowMatrix().ToMatrixString();
                str = str.Remove(str.Length - 2);
            }

            return $"({Output.ToString()} # {str})";
        }

        public List<double> Export()
        {
            return new List<double>(Weights.ToArray());
        }

        public void Import(List<double> list)
        {
            Weights.SetValues(list.ToArray());
        }
    }
}
