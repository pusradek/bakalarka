﻿using MathNet.Numerics.LinearAlgebra.Double;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork.NeuralNetNamespace.Layers
{
    interface ILayer
    {
        int Size { get; }
        ILayer PrevLayer { get; }
        ILayer NextLayer { get; set; }

        void Evaluate(DenseVector inputVector);
        void Propagate(DenseVector error);
        void AdjustWeights();
        List<List<double>> Export();
        void Import(List<List<double>> list);
    }
}
