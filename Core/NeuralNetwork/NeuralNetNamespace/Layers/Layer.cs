﻿using MathNet.Numerics.LinearAlgebra.Double;
using NeuralNetwork.NeuralNetNamespace.Neurons;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork.NeuralNetNamespace.Layers
{
    class Layer : ILayer
    {
        public Layer(int size, int outputs, ILayer prevLayer)
        {
            if (size <= 0)
                throw new ArgumentException("layer size must be greater than zero");

            PrevLayer = prevLayer;
            Neurons = new List<INeuron>(size);

            int weightCount = prevLayer?.Size ?? size;
            for (int i = 0; i < size; i++)
                Neurons.Add(new Neuron(weightCount, outputs));

            OutputVector = new DenseVector(this.Size);
        }

        private List<INeuron> Neurons;

        public int Size => Neurons.Count;

        public ILayer PrevLayer { get; set; }
        public ILayer NextLayer { get; set; }

        public double Result => Neurons[0].Output;

        private DenseVector OutputVector;

        public void Evaluate(DenseVector inputVector)
        {
            for (int i = 0; i < Size; i++)
                OutputVector[i] = Neurons[i].Evaluate(inputVector);

            if (NextLayer != null)
                NextLayer.Evaluate(OutputVector);
        }

        public override string ToString()
        {
            string str = "l: ";
            for (int i = 0; i < Size; i++)
                str += Neurons[i].ToString() + "  ";

            return str;
        }

        public void AdjustWeights()
        {
            foreach (INeuron neuron in Neurons)
                neuron.AdjustWeights();
            if (NextLayer != null)
                NextLayer.AdjustWeights();
        }

        public void Propagate(DenseVector error)
        {
            DenseVector layerError = new DenseVector(PrevLayer?.Size ?? Size);

            for (int i = 0; i < Size; i++)
                layerError += Neurons[i].Propagate(error[i]);

            layerError /= Size;

            if (PrevLayer != null)
                PrevLayer.Propagate(layerError);
        }

        public List<List<double>> Export()
        {
            List<List<double>> layer = new List<List<double>> (Neurons.Count);
            for (int i = 0; i < Neurons.Count; i++)
                layer.Add(Neurons[i].Export());
            return layer;
        }

        public void Import(List<List<double>> layers)
        {
            for (int i = 0; i < layers.Count; i++)
                Neurons[i].Import(layers[i]);
        }
    }
}
