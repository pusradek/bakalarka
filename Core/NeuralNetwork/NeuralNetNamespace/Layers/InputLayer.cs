﻿using System;
using System.Collections.Generic;
using System.Text;
using MathNet.Numerics.LinearAlgebra.Double;
using NeuralNetwork.NeuralNetNamespace.BusinessLogic;
using NeuralNetwork.NeuralNetNamespace.Neurons;

namespace NeuralNetwork.NeuralNetNamespace.Layers
{
    class InputLayer : ILayer
    {
        private DenseVector Input;
        private double BIAS = 0.01;
        
        public InputLayer(int size)
        {
            if (size <= 0)
                throw new ArgumentException("layer size must be greater than zero");

            Size = size;
        }

        public int Size { get; private set; }

        public ILayer PrevLayer => null;

        public ILayer NextLayer { get; set; }

        public void AdjustWeights()
        {
            NextLayer.AdjustWeights();
        }

        public void Evaluate(DenseVector inputVector)
        {
            Input = inputVector;
            NextLayer.Evaluate(inputVector);
        }

        public List<List<double>> Export()
        {
            return new List<List<double>> { new List<double> { Size } };
        }

        public void Import(List<List<double>> list)
        {
            throw new NotImplementedException();
        }

        public void Propagate(DenseVector error)
        {
            return;
        }

        public override string ToString()
        {
            return "Input: " + (Input?.ToString() ?? "empty");
        }
    }
}
