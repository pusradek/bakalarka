﻿using MathNet.Numerics.LinearAlgebra.Double;
using NeuralNetwork.NeuralNetNamespace.BusinessLogic;
using NeuralNetwork.NeuralNetNamespace.Layers;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork.NeuralNetNamespace
{
    public class NeuralNet
    {
        private List<ILayer> layers;

        #region create/import/eport
        public NeuralNet()
        {
            layers = new List<ILayer>();
        }

        public NeuralNet CreateEmpty(List<int> layerSizes)
        {
            layers = new List<ILayer>(layerSizes.Count);

            ILayer prevLayer = new InputLayer(layerSizes[0]);
            layers.Add(prevLayer);

            for (int i = 1; i < layerSizes.Count - 1; i++)
            {
                Layer layer = new Layer(layerSizes[i], layerSizes[i + 1], prevLayer);
                layers.Add(layer);

                prevLayer.NextLayer = layer;
                prevLayer = layer;
            }

            Layer outputLayer = new Layer(layerSizes[layerSizes.Count - 1], 1, prevLayer);
            prevLayer.NextLayer = outputLayer;
            layers.Add(outputLayer);

            return this;
        }

        /// <summary>
        /// weights for neurons in corresponding layers
        /// (net 1:N > layer 1:N > neuron 1:N > weight)
        /// </summary>
        /// <returns>weigts for individual neurons (+layers)</returns>
        public List<List<List<double>>> ExportNet()
        {
            List<List<List<double>>> net = new List<List<List<double>>>(layers.Count);
            for (int i = 0; i < layers.Count; i++)
                net.Add(layers[i].Export());

            return net;
        }

        public void Import(List<List<List<double>>> net)
        {
            //instance initialization first
            List<int> shape = new List<int>(net.Count);

            shape.Add((int)net[0][0][0]);
            for (int i = 1; i < net.Count; i++)
                shape.Add(net[i].Count);

            CreateEmpty(shape);

            //fill with data

            for (int i = 1; i < net.Count; i++)
                layers[i].Import(net[i]);
        }
        #endregion

        public double LastResult { get; private set; }

        public bool EvaluateInput(List<double> input)
        {
            if (input.Count > layers[0].Size)
                throw new ArgumentException($"Layer must have at least {layers[0].Size} elements");

            layers[0].Evaluate(DenseVector.OfEnumerable(input));

            LastResult = ((Layer)layers[layers.Count - 1]).Result;
            return LastResult < 0 ? true : false;
        }

        /// <summary>
        /// training Batch input
        /// </summary>
        /// <param name="input"></param>
        public void BatchInput(IInput input)
        {
            while (input.GetNext())
            {
                EvaluateInput(input.Input);
                ReferenceResult((bool)input.Result);
                if (input.SeekEnd)
                    AdjustWeights();
            }
        }

        /// <summary>
        /// print deitals of each individual neuron
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string str = string.Empty;
            for (int i = 0; i < layers.Count; i++)
                str += layers[i].ToString() + "\n";

            str += $"Result: {LastResult}\n";
            return str;
        }

        /// <summary>
        /// set desired output and propagate it to the network
        /// </summary>
        /// <remarks>note: only propagation not for learning!
        /// for learning <see cref="BatchInput"/>
        /// </remarks>
        /// <param name="reference">desired output</param>
        internal void ReferenceResult(bool reference)
        {
            // Pozn.: 1/2 * (prediction - actual)^2

            double error = LastResult - (reference ? 1 : -1);
            double[] errorArray = new double[] { error };

            layers[layers.Count - 1].Propagate(new DenseVector(errorArray));
        }

        /// <summary>
        /// adjust weights inside neurons (based on previous inputs)
        /// </summary>
        internal void AdjustWeights()
        {
            layers[0].AdjustWeights();
        }

        
    }
}
