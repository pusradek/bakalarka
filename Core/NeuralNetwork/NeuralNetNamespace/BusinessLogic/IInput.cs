﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork.NeuralNetNamespace.BusinessLogic
{
    public interface IInput
    {
        /// <summary>
        /// input data
        /// </summary>
        List<double> Input { get; }

        /// <summary>
        /// get another set of data
        /// </summary>
        /// <returns>if any other data exists (false = end of data)</returns>
        bool GetNext();

        /// <summary>
        /// reference result
        /// null if process evaluates data
        /// </summary>
        bool? Result { get; }

        /// <summary>
        /// End of learning block
        /// </summary>
        bool SeekEnd { get; }
    }
}
