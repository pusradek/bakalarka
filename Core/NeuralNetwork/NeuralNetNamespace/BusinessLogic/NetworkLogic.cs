﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetwork.NeuralNetNamespace.BusinessLogic
{
    static class NetworkLogic
    {
        /// <summary>
        /// Default activation function
        /// </summary>
        public static double ActivationFunction(double value)
        {
            return Math.Tanh(value);
        }

        /// <summary>
        /// derivative of default activation function
        /// </summary>
        /// <param name="value">value from activationFunction</param>
        public static double DerivationFunction(double value)
        {
            return TanhDerivation(value);
        }

        public static double TanhDerivation(double value)
        {
            return 1 - (value * value);
        }

        public static double Tanh(double value)
        {
            return Math.Tanh(value);
        }

        public static double Sigmoid(double value)
        {
            return value / (1 + Math.Abs(value));
        }

        public static double SigmoidDerivation(double value)
        {
            return value * (1 - value);
        }

        public static double ReLu(double value)
        {
            if (value <= 0) return 0;
            if (value >= 1) return 1;
            return value;
        }
    }
}
