﻿using NeuralNetwork.NeuralNetNamespace;
using System;
using System.Collections.Generic;

namespace NeuralNetwork
{
    class Program
    {
        private static int startSize;
        static void Main(string[] args)
        {
            NeuralNet ann = TestTrain(infinite: true);

            Console.WriteLine("evaluate trained:");
            EvaluationTest(ann);

            /*Console.WriteLine("evaluate trained 2:");
            EvaluationTest(ann);

            Console.WriteLine("exporting:");
            List<List<List<double>>> export = ann.ExportNet();
            Console.WriteLine("done");

            Console.WriteLine("evaluate trained 3:");
            EvaluationTest(ann);

            Console.WriteLine("importing to new net:");
            NeuralNet import = new NeuralNet();
            import.Import(export);
            Console.WriteLine("done");

            Console.WriteLine("test:");
            EvaluationTest(import);
            Console.WriteLine("done");

            Console.WriteLine("importing to old net:");
            ann.Import(export);
            Console.WriteLine("done");

            Console.WriteLine("test:");
            EvaluationTest(ann);
            Console.WriteLine("done");*/
        }

        private static KeyValuePair<bool, List<double>> GetGenerator()
        {
            return XORGenerator();
            //return OddEvenGenerator();
        }

        private static void EvaluationTest(NeuralNet ann)
        {
            int passed = 0;
            int failed = 0;
            double yes = 0;
            double predictionYes = 0;
            double no = 0;
            double predictionNo = 0;
            const int iterations=10000;
            for (int i = 0; i < iterations; i++)
            {
                var data = GetGenerator();
                bool guess = ann.EvaluateInput(data.Value);
                bool result = data.Key;

                if (guess == result)
                    passed++;
                else
                    failed++;

                if (result)
                    yes++;
                else
                    no++;

                if (guess)
                    predictionYes++;
                else
                    predictionNo++;
            }

            double localResult = ((double)passed / iterations) * 100;
            Console.WriteLine($"succes rate: {localResult}% (p:{passed} vs. f:{failed})");
            Console.WriteLine($"Actualyes: {yes}, Actualno:{no}");
            Console.WriteLine($"predictionYes: {predictionYes}, predictionNo:{predictionNo}");
            Console.WriteLine("_-_-_-_-_-_-_-_-_-_-_-_-_");
        }

        private static NeuralNet TestTrain(bool infinite)
        {
            GetGenerator();
            List<int> widths = new List<int>() { startSize, 4 ,4,2, 1 };
            NeuralNet ANN = new NeuralNet().CreateEmpty(widths);


            Console.WriteLine(ANN.ToString());
            Console.WriteLine("=================================");

            int passed = 0;
            int failed = 0;
            double best = 0;
            double worst = 101;
            double yes = 0;
            double predictionYes = 0;
            double no = 0;
            double predictionNo = 0;
            const int step = 100000;
            const int adjust = 100;
            //const int step = 20;
            //const int adjust = 3;
            for (int i = 0; infinite || i < 100000; i++)
            {
                var data = GetGenerator();
                bool guess = ANN.EvaluateInput(data.Value);
                bool result = data.Key;

                if (guess == result)
                    passed++;
                else
                    failed++;

                if (result)
                    yes++;
                else
                    no++;

                if (guess)
                    predictionYes++;
                else
                    predictionNo++;

                ANN.ReferenceResult(result);
                if (i % adjust == 0 && i > adjust)
                    ANN.AdjustWeights();

                if (i>1 && i % step == 0)
                {
                    Console.WriteLine(ANN.ToString());
                    Console.WriteLine("_-_-_-_-_-_-_-_-_-_-_-_-_");
                    double localResult = ((double)passed / step) * 100;
                    if (localResult > best)
                        best = localResult;
                    if (localResult < worst && localResult > 1)
                        worst = localResult;
                    Console.WriteLine($"succes rate: {localResult}% (p:{passed} vs. f:{failed})");
                    Console.WriteLine($"Actualyes: {yes}, Actualno:{no}");
                    Console.WriteLine($"predictionYes: {predictionYes}, predictionNo:{predictionNo}");
                    Console.WriteLine($"best: {best}%, worst: {worst}");
                    Console.WriteLine($"guess: {guess}, result:{result}");
                    Console.WriteLine("_-_-_-_-_-_-_-_-_-_-_-_-_");
                    predictionYes = predictionNo = yes = no = passed = failed = 0;
                }

            }

            return ANN;
        }

        private static KeyValuePair<bool, List<double>> XORGenerator()
        {
            startSize = 2;
            switch (new Random().Next(0, 3))
            {
                case 0: return new KeyValuePair<bool, List<double>>(false, new List<double> { -1, -1 });
                case 1: return new KeyValuePair<bool, List<double>>(true, new List<double> { 1, -1 });
                case 2: return new KeyValuePair<bool, List<double>>(true, new List<double> { -1, 1 });
                case 3: return new KeyValuePair<bool, List<double>>(false, new List<double> { 1, 1 });
                default:
                    throw new ArgumentException("unknown number");
            }
        }

        private static KeyValuePair<bool, List<double>> OddEvenGenerator()
        {
            startSize = 3;
            switch (new Random().Next(0, 6))
            {
                case 0: return new KeyValuePair<bool, List<double>>(true, new List<double> { -1, -1, -1 });
                case 1: return new KeyValuePair<bool, List<double>>(false, new List<double> { 1, -1, -1 });
                case 2: return new KeyValuePair<bool, List<double>>(true, new List<double> { -1, 1, -1 });
                case 3: return new KeyValuePair<bool, List<double>>(false, new List<double> { 1, 1, -1 });
                case 4: return new KeyValuePair<bool, List<double>>(true, new List<double> { -1, -1, 1 });
                case 5: return new KeyValuePair<bool, List<double>>(false, new List<double> { 1, -1, 1 });
                case 6: return new KeyValuePair<bool, List<double>>(true, new List<double> { -1, 1, 1 });
                case 7: return new KeyValuePair<bool, List<double>>(false, new List<double> { 1, 1, 1 });
                default:
                    throw new ArgumentException("unknown number");
            }
        }
    }
}
