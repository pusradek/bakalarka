RESTORE DATABASE Recommneder
FROM DISK = '/app/Recommneder.bak'
WITH REPLACE,
	MOVE 'Recommender' TO '/app/data/Recommender.mdf',
	MOVE 'Recommender_log' TO '/app/data/Recommender_log.ldf'